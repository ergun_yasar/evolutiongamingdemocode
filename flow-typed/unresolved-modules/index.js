declare module '@fortawesome/free-brands-svg-icons' {
    declare module.exports: any;
}

declare module '@fortawesome/free-solid-svg-icons' {
    declare module.exports: any;
}

declare module '@fortawesome/react-fontawesome' {
    declare module.exports: any;
}

declare module 'immutability-helper' {
    declare module.exports: any;
}

declare module 'redux-thunk' {
    declare module.exports: any;
}

declare module '@storybook/addon-backgrounds' {
    declare module.exports: any;
}

declare module '@storybook/addon-centered' {
    declare module.exports: any;
}

declare module '@storybook/addon-centered' {
    declare module.exports: any;
}