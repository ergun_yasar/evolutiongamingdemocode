// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({component: Component, ...restProps}: any) => {
    return (
        <Route {...restProps} render={(props) => {
            console.log(props);
            return (
                restProps.isAuthenticated
                    ?
                    <Component {...props}/>
                    :
                    <Redirect to={{
                        pathname: '/login',
                        state: { from: props.location }
                    }} />
            )
        }}/>
    )
};

PrivateRoute.propTypes = {
    authenticationRoute: PropTypes.string,
    isAuthenticated: PropTypes.bool.isRequired
};

export default PrivateRoute