// @flow
import React from 'react';
import constants from '../../constants/index';
import Logo from "../common/logo";

const HeaderBrand = () => {
    return (
        <div className={'header-brand-container'}>
            <Logo />
            <span className={'header-brand-container__brand'}>
                {
                    constants.APP.BRAND
                }
            </span>
        </div>
    )
};

export default HeaderBrand;