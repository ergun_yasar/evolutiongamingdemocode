// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import type {Dispatch} from 'redux';
import {connect} from 'react-redux';

import NetworkIcon from '../../assets/icons/navigation/network.png';
import TicketsIcon from '../../assets/icons/navigation/tickets.png';
import MessagesIcon from '../../assets/icons/navigation/messages.png';
import MatchIcon from '../../assets/icons/navigation/match.png';
import HeaderNavigationItem from './headerNavigationItem';
import {NAVIGATION_ITEM_TYPES} from '../../constants/enums';
import DropdownMenu from '../common/dropdownMenu/dropdownMenu';
import DropdownMenuProfileItem from '../common/dropdownMenu/DropdownMenuProfileItem';
import DropdownMenuDivider from '../common/dropdownMenu/dropdownMenuDivider';
import DropdownMenuItems from '../common/dropdownMenu/dropdownMenuItems';
import DropdownMenuItem from '../common/dropdownMenu/DropdownMenuItem';
import authenticationActionCreator from '../../actionCreators/authenticationActionCreator';

class HeaderNavigation extends Component<any, any> {

    onSignOutPress = () => {
        this.props.authenticationActions.logout();
    };

    render() {
        const { has_profile_image, profile_image, name, title, extended_data } = this.props.entity;
        //alert(this.props.location.pathname);
        return (
            <div className={'header-navigation'}>
                <HeaderNavigationItem
                    type={NAVIGATION_ITEM_TYPES.title} title={'Home'}
                    isSelectedItem={this.props.location.pathname === '/someCompany' || this.props.location.pathname === '/someCompany/' || this.props.location.pathname === '/' || this.props.location.pathname === '/home'}
                    to={'/'}
                />
                <HeaderNavigationItem
                    type={NAVIGATION_ITEM_TYPES.icon} title={'Network'} iconSource={NetworkIcon}
                    // badgeCount={5}
                    isSelectedItem={this.props.location.pathname === '/network' || this.props.location.pathname === '/requests'}
                    to={'/network'}
                />
                <HeaderNavigationItem
                    type={NAVIGATION_ITEM_TYPES.icon} title={'Tickets'} iconSource={TicketsIcon}
                    isSelectedItem={this.props.location.pathname === '/tickets'}
                    to={'/tickets'}
                />
                {/*<HeaderNavigationItem type={NAVIGATION_ITEM_TYPES.icon} title={'Messages'} iconSource={MessagesIcon}
                                  isSelectedItem={props.location.pathname === '/messages'}
                                  to={'/messages'}/>*/}
                {/*<HeaderNavigationItem*/}
                    {/*type={NAVIGATION_ITEM_TYPES.icon} title={'Matching'} iconSource={MatchIcon}*/}
                    {/*isSelectedItem={this.props.location.pathname === '/match'}*/}
                    {/*to={'/match'}*/}
                {/*/>*/}
                <HeaderNavigationItem
                    type={NAVIGATION_ITEM_TYPES.image} title={'Profile'}
                    imageURL={'https://randomuser.me/api/portraits/men/9.jpg'}
                    isSelectedItem={this.props.location.pathname === '/profile'}
                    to={'/profile'}
                >
                    <DropdownMenu>
                        <DropdownMenuProfileItem
                            imageSource={has_profile_image ? profile_image['50x50'] : ''}
                            text={name}
                            subText={`${title}${extended_data.hasOwnProperty('company') ? ' at ' + extended_data['company'] : ''}`}
                            viewLink={'/profile'}
                        />
                        <DropdownMenuDivider/>
                        <DropdownMenuItems>
                            <DropdownMenuItem label={'Settings & Privacy'} onClick={() => {}}/>
                            <DropdownMenuItem label={'Help Center'} onClick={() => {}}/>
                            <DropdownMenuItem label={'Language'} onClick={() => {}}/>
                        </DropdownMenuItems>
                        <DropdownMenuDivider/>
                        <DropdownMenuItems>
                            <DropdownMenuItem label={'Sign Out'} onClick={this.onSignOutPress}/>
                        </DropdownMenuItems>
                    </DropdownMenu>
                </HeaderNavigationItem>

            </div>
        );
    }
}

HeaderNavigation.propTypes = {};

const mapStateToProps = (state) => {
    return {
        entity: state.account.entity
    }
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {
        authenticationActions: bindActionCreators(authenticationActionCreator, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderNavigation)