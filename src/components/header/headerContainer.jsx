// @flow
import React from 'react';
import HeaderBrand from './headerBrand';
import HeaderNavigation from "./headerNavigation";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const HeaderContainer = (props: any) => {
    return (
        <div className={"header-container"}>
            <div>
                <HeaderBrand/>
                <div className={'header-search '}>
                    <FontAwesomeIcon icon={faSearch} className={'header-search__icon'} />
                    <input type={'text'} className={'header-search__input'} placeholder={'Search...'}/>
                </div>
                <HeaderNavigation location={props.location}/>
                <div className={'header__hamburger-menu'}>
                    <button type={'button'} >
                        <img src={'https://randomuser.me/api/portraits/men/9.jpg'} className={'header__hamburger-menu__image'}>

                        </img>
                    </button>
                    <button type={'button'}>
                        <img src={require('../../assets/icons/hamburger_menu_icon.png')} className={'header__hamburger-menu__icon'}>

                        </img>
                    </button>
                </div>

            </div>
        </div>
    );
};

export default HeaderContainer;

