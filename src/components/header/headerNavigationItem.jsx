// @flow
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { NAVIGATION_ITEM_TYPES } from "../../constants/enums";
import Badge from "../common/badge";
import { Link } from "react-router-dom";

const render = (props: any) => {
    const { type, imageURL, iconSource, title, onClick, badgeCount, isSelectedItem } = props;
    switch ( type ) {
        case NAVIGATION_ITEM_TYPES.image: {
            return renderWithImage(imageURL, title, onClick, badgeCount, isSelectedItem);
        }
        case NAVIGATION_ITEM_TYPES.icon: {
            return renderWithIcon(iconSource, title, onClick, badgeCount, isSelectedItem);
        }
        default: {
            return renderWithTitle(title, onClick, badgeCount, isSelectedItem)
        }
    }
};

const renderWithImage = (imageURL, title, onClick, badgeCount, isSelectedItem) => (
    <div className={'header-navigation-item'}>
        <img src={imageURL} className={'header-navigation-item__image'}/>
        <span className={classnames('header-navigation-item__sub-text', { 'header-navigation-item__sub-text--bold': isSelectedItem })}>
            {
                title
            }
        </span>
        {
            renderBadgeCount(badgeCount)
        }
    </div>
);

const renderWithIcon = (iconSource, title, onClick, badgeCount, isSelectedItem) => (
    <div className={'header-navigation-item'}>
        <img src={iconSource} className={'header-navigation-item__icon'}/>
        <span className={classnames('header-navigation-item__sub-text', { 'header-navigation-item__sub-text--bold': isSelectedItem })}>
            {
                title
            }
        </span>
        {
            renderBadgeCount(badgeCount)
        }
    </div>
);

const renderWithTitle = (title, onClick, badgeCount, isSelectedItem) => (
    <button type={'button'} className={'header-navigation-item'} onClick={onClick}>
        <span className={classnames('header-navigation-item__text', { 'header-navigation-item__sub-text--bold': isSelectedItem })}>
            {
                title
            }
        </span>
        {
            renderBadgeCount(badgeCount)
        }
    </button>
);

const renderBadgeCount = (badgeCount) => {
    if ( badgeCount <= 0 ) {
        return null;
    }

    return (
        <Badge badgeCount={badgeCount}/>
    )
};

const HeaderNavigationItem = (props: any) => {
    if (props.children) {
        return (
            <button type={'button'}>
                {
                    render(props)
                }
                {
                    props.children
                }
            </button>
        )
    }
    return (
        <Link to={props.to}>
            {
                render(props)
            }
            {
                props.children
            }
        </Link>
    );
};

HeaderNavigationItem.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    imageURL: PropTypes.string,
    iconSource: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.oneOf([NAVIGATION_ITEM_TYPES.icon, NAVIGATION_ITEM_TYPES.image, NAVIGATION_ITEM_TYPES.title]),
    onClick: PropTypes.func,
    badgeCount: PropTypes.number,
    isSelectedItem: PropTypes.bool,
    to: PropTypes.string,
};

HeaderNavigationItem.defaultProps = {
    children: null,
    type: 'title',
    imageURL: '',
    iconSource: null,
    title: '',
    onClick: () => {
    },
    badgeCount: 0,
    isSelectedItem: false
};

export default HeaderNavigationItem;