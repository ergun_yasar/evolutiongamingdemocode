// @flow
import React from 'react';
import PropTypes from 'prop-types';
import BorderedButton from "../common/borderedButton";

import theme from '../../theme/theme';

const { GREEN, STORM_GRAY } = theme.colors;

const AcceptAndDeclineButtons = (props: any) => {
    return (
        <div className={'accept-and-decline-buttons'}>
            <BorderedButton
                label={'Accept'}
                height={'35px'}
                borderRadius={'10.4px'}
                labelFontSize={'0.875em'}
                labelColor={GREEN}
                borderColor={GREEN}
                onClick={props.onAcceptClick}
                padding={'0px 12px'}
            />
            <BorderedButton
                label={'Decline'}
                height={'35px'}
                borderRadius={'10.4px'}
                labelFontSize={'0.875em'}
                labelColor={STORM_GRAY}
                borderColor={STORM_GRAY}
                onClick={props.onDeclineClick}
                padding={'0px 12px'}
            />
        </div>
    )
};

AcceptAndDeclineButtons.propTypes = {
    onAcceptClick: PropTypes.func,
    onDeclineClick: PropTypes.func,
};

AcceptAndDeclineButtons.defaultProps = {
    onAcceptClick: () => {},
    onDeclineClick: () => {}
};

export default AcceptAndDeclineButtons;