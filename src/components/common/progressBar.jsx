// @flow
import React from 'react';
import PropTypes from 'prop-types';

const ProgressBar = (props: any) => (
    <div className={'common__progress-bar'}>
        <div className={'common__progress-bar--active'}
             style={{ flex: props.currentStep / props.totalSteps }}>
        </div>
        <div style={{ flex: (props.totalSteps - props.currentStep) / props.totalSteps }}>

        </div>
    </div>
);

ProgressBar.propTypes = {
    currentStep: PropTypes.number.isRequired,
    totalSteps: PropTypes.number.isRequired,
};

export default ProgressBar;