// @flow
import React from 'react';
import PropTypes from 'prop-types';

const ExhibitorItem = (props: any) => {
    return (
        <div className={'exhibitor-item'}>
            <img src={props.imageSource} className={'exhibitor-item__image'}/>
            <div className={'exhibitor-item__content-container'}>
                <span className={'exhibitor-item__title'}>
                    {
                        props.title
                    }
                </span>
                <span className={'exhibitor-item__subtitle'}>
                    {
                        props.subTitle
                    }
                </span>
                <a href={'#'} className={'exhibitor-item__read-more'}>
                    Read more >
                </a>
            </div>
        </div>
    )
};

ExhibitorItem.propTypes = {
    imageSource: PropTypes.string,
    subTitle: PropTypes.string,
    title: PropTypes.string,
};

ExhibitorItem.defaultProps = {

};

export default ExhibitorItem;