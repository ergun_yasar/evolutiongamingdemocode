// @flow
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Card = (props: any) => (
    <div
        className={classnames('card', props.classNames)}
        onClick={props.onClick}
        style={props.style}
    >
        {
            props.children
        }
    </div>
);

Card.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    classNames: PropTypes.object,
    onClick: PropTypes.func,
    style: PropTypes.object,
};

export default Card;