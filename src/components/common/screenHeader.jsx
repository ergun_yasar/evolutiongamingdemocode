// @flow
import React, {Component} from 'react';
import update from 'immutability-helper';

const ScreenHeader = (props: any) => {
    return (
        <div className={'screen-header'}>
            <div className={'screen-header__body'}>
                {
                    props.children
                }
            </div>
        </div>
    )
};

ScreenHeader.propTypes = {};

ScreenHeader.defaultProps = {};

export default ScreenHeader