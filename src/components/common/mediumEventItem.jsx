// @flow
import React from 'react';
import PropTypes from 'prop-types';
import EventDate from "./eventDate";

const MediumEventItem = (props: any) => {
    return (
        <div className={'medium-event-item'}>
            <div className={'medium-event-item__image-wrapper'}>
                <div>
                    <img src={props.imageSource} className={'medium-event-item__image'}/>
                </div>
            </div>
            <div style={{ display: 'block' }}>
                <div className={'medium-event-item__content-container'}>
                    <div className={'medium-event-item__event-date'}>
                        <EventDate month={'SEP'} day={'21'}/>
                    </div>
                    <div className={'medium-event-item__info-container'}>
                    <span className={'medium-event-item__name'}>
                        {
                            props.name
                        }
                    </span>
                        <span className={'medium-event-item__date'}>
                        {
                            props.date
                        }
                    </span>
                        <span className={'medium-event-item__location'}>
                        {
                            props.location
                        }
                    </span>
                    </div>
                </div>
            </div>
        </div>
    )
};

MediumEventItem.propTypes  = {
    imageSource: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.string,
    location: PropTypes.string,
};

MediumEventItem.defaultProps = {

};

export default MediumEventItem;