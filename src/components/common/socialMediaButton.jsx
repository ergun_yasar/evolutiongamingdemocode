// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import theme from '../../theme/theme';
const { WHITE } = theme.colors;

const SocialMediaButton = (props: any) => {
    return (
        <button type={'button'} onClick={props.onClick} className={'common__social-media-button'} style={Object.assign({ backgroundColor: props.labelBackgroundColor }, props.style)}>
            <div className={'common__social-media-button__icon'} style={{ backgroundColor: props.iconBackgroundColor }}>
                <div className={'common__social-media-button__icon__image__wrapper'}>
                    <FontAwesomeIcon icon={props.iconSource} className={'common__social-media-button__icon__image'}/>
                </div>
            </div>
            <span className={'common__social-media-button__label'} style={{ color: props.labelColor }}>
                {
                    props.label
                }
            </span>
        </button>
    )
};

SocialMediaButton.propTypes = {
    iconSource: PropTypes.object.isRequired,
    iconBackgroundColor: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    labelBackgroundColor: PropTypes.string.isRequired,
    labelColor: PropTypes.string,
    onClick: PropTypes.func,
    style: PropTypes.object,
};

SocialMediaButton.defaultProps = {
    labelColor: WHITE,
    onClick: () => {},
    style: {}
};

export default SocialMediaButton;