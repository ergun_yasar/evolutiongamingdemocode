// @flow
import React from 'react';

import logo from '../../assets/images/logo.png';

const Logo = () => {
    return (
        <img src={logo} className={'logo'}/>
    );
};

export default Logo;

