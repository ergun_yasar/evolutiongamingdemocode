// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import Card from "./card";
import CardBody from "./cardBody";
import RoundImage from "./roundImage";

const Alert = (props: any) => {
    return (
        <Card classNames={{ 'alert': true, 'alert--info': true }}>
            <CardBody padding={'5px 60px'}>
                <div className={'alert__body'}>
                    <RoundImage width={'78px'} height={'78px'} src={props.imageSource}/>
                    <span className={'alert__label'}>
                        <span style={{ fontSize: '16px' }}>
                            <span className={'alert__label alert__label--strong'}>
                                {
                                    props.strongLabel
                                }
                            </span>
                        </span>
                        &nbsp;
                        {
                            props.label
                        }
                    </span>
                </div>
            </CardBody>
        </Card>
    )
}

Alert.propTypes = {
    imageSource: PropTypes.string,
    strongLabel: PropTypes.string,
    label: PropTypes.string,
};

Alert.defaultProps = {};

export default Alert