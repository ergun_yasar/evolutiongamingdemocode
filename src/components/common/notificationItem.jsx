// @flow
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import RoundImage from "./roundImage";
import AcceptAndDeclineButtons from "../buttons/acceptAndDeclineButtons";

const NotificationItem = (props: any) => {
    return (
        <div className={'notification-item'}>
            <RoundImage width={'60px'} height={'60px'} src={props.imageSource}/>
            <div className={classnames('notification-item__container', { 'notification-item__container--horizontal': props.showHorizontal })}>
                <span className={'notification-item__title'}>
                    {
                        `${props.title},`
                    }
                    &nbsp;
                </span>
                <span className={'notification-item__text'}>
                    {
                        props.text
                    }
                </span>
            </div>
            <AcceptAndDeclineButtons/>
        </div>
    )
};

NotificationItem.propTypes = {
    imageSource: PropTypes.string,
    text: PropTypes.string,
    title: PropTypes.string,
    onAcceptClick: PropTypes.func,
    onDeclineClick: PropTypes.func,
    showHorizontal: PropTypes.bool,
};

NotificationItem.defaultProps = {
    imageSource: ''
};

export default NotificationItem;