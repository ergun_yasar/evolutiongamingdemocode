// @flow
import React, {Component} from 'react';
import Logo from '../../../assets/images/logo.png';

const loading = () => {
    return (
        <div className={'loading'}>
            <img className={'loading__image'} alt={''} src={Logo}/>
        </div>
    );
};

export default loading;