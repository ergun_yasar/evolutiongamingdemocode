// @flow
import React from 'react';
import PropTypes from 'prop-types';

const InformationItem = (props: any) => {
    return (
        <div className={'common__information-item'}>
            <div className={'common__information-item__icon-wrapper'}>
                <img src={props.iconSource} className={'common__information-item__icon'}/>
            </div>
            <div className={'common__information-item__label-container'}>
                <span className={'common__information-item__label'}>
                    {
                        props.label
                    }
                    <span className={'common__information-item__label--strong'}>
                        {
                            props.strongLabel
                        }
                    </span>
                </span>
            </div>
        </div>
    );
};

InformationItem.propTypes = {
    iconSource: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    strongLabel: PropTypes.string,
};

InformationItem.defaultProps = {

};

export default InformationItem;