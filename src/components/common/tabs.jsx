// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type {Dispatch} from 'redux';
import update from 'immutability-helper';

class Tabs extends Component<any> {
    static defaultProps: any;
    constructor(...args) {
        super(...args)
    }

    render() {
        return (
            <div>

            </div>
        );
    }
}

Tabs.propTypes = {
    activeTabIndex: PropTypes.number,
    tabs: PropTypes.arrayOf(PropTypes.shape({
        iconSource: PropTypes.string,
        title: PropTypes.string.isRequired,
        badgeCount: PropTypes.number,
    }))
};

Tabs.defaultProps = {};

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Tabs)