// @flow
import React from 'react';
import PropTypes from 'prop-types';

const CardHeader = (props: any) => {
    return (
        <div className={'card-header'}>
            <span className={'card-header__title'}>
                {
                    props.title
                }
            </span>
            <span className={'card-header__right-label'}>
                {
                    props.rightLabel
                }
            </span>
        </div>
    )
};

CardHeader.propTypes = {
    title: PropTypes.string.isRequired,
    rightLabel: PropTypes.string,
};

CardHeader.defaultProps = {
    rightLabel: '',
};

export default CardHeader;