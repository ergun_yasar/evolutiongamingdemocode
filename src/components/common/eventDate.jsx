// @flow
import React from 'react';
import PropTypes from 'prop-types';

const EventDate = (props: any) => {
    return (
        <div className={'event-date'}>
            <span className={'event-date__month'}>
                {
                    props.month
                }
            </span>
            <span className={'event-date__day'}>
                {
                    props.day
                }
            </span>
        </div>
    )
};

EventDate.propTypes = {
    day: PropTypes.string,
    month: PropTypes.string,
};

export default EventDate;

