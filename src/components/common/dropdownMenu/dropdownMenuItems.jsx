// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';

const DropdownMenuItems = (props:any) => {
    return (
        <div className={'dropdown-menu__items-container'}>
            {
                props.children
            }
        </div>
    );
};

DropdownMenuItems.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
};

export default DropdownMenuItems;