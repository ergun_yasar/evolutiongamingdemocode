// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import RoundImage from '../roundImage';
import {Link} from 'react-router-dom';

const DropdownMenuProfileItem = (props:any) => {
    return (
        <div className={'dropdown-menu__profile-container'}>
            <div className={'dropdown-menu__profile-wrapper'}>
                <RoundImage width={'32px'} height={'32px'} src={props.imageSource}/>
                <div className={'dropdown-menu__profile-info-container'}>
                    <span className={'dropdown-menu__profile-text'}>
                        {
                            props.text
                        }
                    </span>
                    <span className={'dropdown-menu__profile-sub-text'}>
                        {
                            props.subText
                        }
                    </span>
                </div>
            </div>
            <div className={'dropdown-menu__profile-view-link-container'}>
                <Link to={props.viewLink}>
                    <span className={'dropdown-menu__profile-view-link'}>
                        View Profile
                    </span>
                </Link>

            </div>
        </div>
    );
};

DropdownMenuProfileItem.propTypes = {
    imageSource: PropTypes.string,
    text: PropTypes.string,
    subText: PropTypes.string,
    viewLink: PropTypes.string.isRequired,
};

DropdownMenuProfileItem.defaultProps = {};

export default DropdownMenuProfileItem;