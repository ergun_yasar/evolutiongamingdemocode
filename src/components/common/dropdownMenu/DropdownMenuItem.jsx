// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';

const DropdownMenuItem = (props: any) => {
    return (
        <button className={'dropdown-menu__item-container'} onClick={props.onClick}>
            <span className={'dropdown-menu__item'}>
                {
                    props.label
                }
            </span>
        </button>
    );
};

DropdownMenuItem.propTypes = {
    label: PropTypes.string,
    onClick: PropTypes.func,
};

DropdownMenuItem.defaultProps = {};

export default DropdownMenuItem;