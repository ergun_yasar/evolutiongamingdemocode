// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';

const DropdownMenu = (props: any) => {
    return (
        <div>
            <div className={'dropdown-menu__diamond'}/>
            <div className={'dropdown-menu'}>
                {
                    props.children
                }
            </div>
        </div>
    );
};

DropdownMenu.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
};

export default DropdownMenu;