// @flow
import React from 'react';
import PropTypes from 'prop-types';

const BasicUserInformationItem = (props: any) => {
    return (
        <div className={'basic-user-information-item'}>
            <div className={'basic-user-information-item__image-wrapper'}>
                <img src={props.imageSource} className={'basic-user-information-item__image'}/>
            </div>
            <div className={'basic-user-information__text-container'}>
                <span className={'basic-user-information__text'}>
                    {
                        `${props.name}, ${props.title} at`
                    }
                    <span style={{fontSize: '16px'}}>
                        <span
                            className={'basic-user-information__text basic-user-information__text--medium'}
                        >
                            {
                                ` ${ props.domain }`
                            }
                        </span>
                    </span>
                </span>
            </div>
        </div>
    )
};

BasicUserInformationItem.propTypes = {
    domain: PropTypes.string,
    imageSource: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
};

BasicUserInformationItem.defaultProps = {};

export default BasicUserInformationItem;