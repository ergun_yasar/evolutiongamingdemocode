// @flow
import React from 'react';
import PropTypes from 'prop-types';

const RoundImage = (props: any) => {
    return (
        <img src={props.src} style={{ width: props.width, height: props.height, borderRadius: '50%' }}/>
    )
};

RoundImage.propTypes = {
    src: PropTypes.any,
    width: PropTypes.string.isRequired,
    height: PropTypes.string.isRequired,
};

export default RoundImage;