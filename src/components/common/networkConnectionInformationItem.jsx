// @flow
import React from 'react';
import PropTypes from 'prop-types';

const NetworkConnectionInformationItem = (props: any) => {
    return (
        <div className={'network-connection-information-item'}>
            <div className={'network-connection-information-item__icon-wrapper'}>
                <img src={props.iconSource} className={'network-connection-information-item__icon-wrapper'}/>
            </div>
            <span className={'network-connection-information-item__text'}>
                {
                    props.text
                }
            </span>
        </div>
    )
};

NetworkConnectionInformationItem.propTypes = {
    iconSource: PropTypes.string,
    text: PropTypes.string,
};

NetworkConnectionInformationItem.defaultProps = {

};

export default NetworkConnectionInformationItem;
