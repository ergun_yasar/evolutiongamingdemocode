// @flow
import React from 'react';
import PropTypes from 'prop-types';

const HighlightItem = (props: any) => {
    return (
        <div className={'highlight-item'}>
            <div className={'highlight-item__icon-wrapper'} style={{ backgroundColor: props.backgroundColor }}>
                <img src={props.iconSource} className={'highlight-item__icon'}/>
            </div>
            <span className={'highlight-item__label'}>
                {
                    props.label
                }
                <span style={{ fontSize: '16px' }}>
                    <span className={'highlight-item__label--strong'}>
                        {
                            props.strongLabel
                        }
                    </span>
                </span>
            </span>
        </div>
    );
};

HighlightItem.propTypes = {
    backgroundColor: PropTypes.string,
    iconSource: PropTypes.string,
    label: PropTypes.string,
    strongLabel: PropTypes.string,
};

HighlightItem.defaultProps = {};

export default HighlightItem;