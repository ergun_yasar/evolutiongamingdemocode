// @flow
export type Option = {
    key: string,
    label: string,
}

export type Props = {
    onChange: Function,
    options: Array<Option>,
    placeholder: string,
    showPlaceholderOption: boolean
}

export type State = {
    showOptions: boolean,
    selectedOption: ?Option
}