// @flow
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import lodash from 'lodash';

import type { Props, State, Option } from './select.type';

class Select extends Component<Props, State> {
    static defaultProps: any;
    optionRefs: Array<any>;
    constructor(...args: any[]) {
        super(...args);

        this.state = {
            showOptions: false,
            selectedOption: null,
        };

        this.optionRefs = [];
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    };

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    };

    getIcon = () => this.state.showOptions ? faCaretDown : faCaretRight;

    getLabel = () => this.state.selectedOption ? this.state.selectedOption.label : this.props.placeholder;

    // to get option via ref
    getSelectedOption = () => this.state.selectedOption;

    handleClickOutside = (event: any) => {
        const domNode = ReactDOM.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target)) {
            this.setState(previousState => update(previousState, {showOptions: {$set: false}}));
        }
    };

    isOptionSelected = (option: Option) => {
        return JSON.stringify(option) === JSON.stringify(this.state.selectedOption)
    };

    onOptionClick = (selectedOption: ?Option) => {
        this.setState(previousState => update(previousState, {
            selectedOption: {$set: selectedOption},
            showOptions: {$set: false}
        }), () => {
            this.props.onChange(selectedOption);
        });
    };

    onSelectClick = () => {
        this.setState((previousState: State) => update(previousState, {showOptions: {$set: !previousState.showOptions}}), () => {
            if (this.state.showOptions) {
                this.scrollToOption();
            }
        });
    };

    renderDropDown = () => {
        return (
            <div className={'select__dropdown'}>
                {
                    this.renderPlaceholderOption()
                }
                {
                    this.props.options.map((item: Option, index: number) => (
                        <div
                            className={classnames('select__option', {'select__option--selected': this.isOptionSelected(item)})}
                            key={item.key}
                            onClick={() => {
                                this.onOptionClick(item)
                            }}
                            ref={(option) => { this.optionRefs[index] = option }}
                        >
                            <span className={'select__option-label'}>
                                {
                                    item.label
                                }
                            </span>
                        </div>
                    ))
                }
            </div>
        )
    };

    renderPlaceholderOption = () => {
        if (this.props.showPlaceholderOption) {
            return (
                <div
                    className={'select__option'}
                    onClick={() => {
                        this.onOptionClick(null)
                    }}
                >
                    <span className={'select__option-label'}>
                        {
                            this.props.placeholder
                        }
                    </span>
                </div>
            )
        }

        return null;
    };

    scrollToOption = () => {
        if (this.state.selectedOption) {
            const optionIndex = lodash.findIndex(this.props.options, this.state.selectedOption);
            if (optionIndex > -1) {
                this.optionRefs[optionIndex].scrollIntoView({block: 'end', behavior: 'instant'});
            }
        }
    };

    render() {
        return (
            <div className={'select'}>
                <div className={'select__label-container'} onClick={this.onSelectClick}>
                    <span className={'select__label'}>
                        {
                            this.getLabel()
                        }
                    </span>
                </div>
                <div className={'select__arrow-container'} onClick={this.onSelectClick}>
                    <FontAwesomeIcon icon={this.getIcon()} className={'select__arrow'}/>
                </div>
                {
                    this.state.showOptions && this.renderDropDown()
                }
            </div>
        );
    }
}

Select.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        key: PropTypes.string,
    })).isRequired,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    showPlaceholderOption: PropTypes.bool,
};

Select.defaultProps = {
    onChange: () => {},
    placeholder: 'Select',
    showPlaceholderOption: false
};

export default Select