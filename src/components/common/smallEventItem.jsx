// @flow
import React from 'react';
import PropTypes from 'prop-types';
import EventDate from "./eventDate";

const SmallEventItem = (props: any) => {
    return (
        <div className={'small-event-item'}>
            <div className={'small-event-item__image'} style={{ backgroundImage: 'url("' + props.imageSource +  '")' }}/>
            <div className={'small-event-item__info-container'}>
                <span className={'small-event-item__name'}>
                    {
                        props.name
                    }
                </span>
                <span className={'small-event-item__detail'}>
                    {
                        props.detail
                    }
                </span>
                <div className={'small-event-item__event-date'}>
                    <EventDate month={props.month} day={props.day}/>
                </div>
            </div>
        </div>
    )
};

SmallEventItem.propTypes = {
    imageSource: PropTypes.string,
    name: PropTypes.string,
    detail: PropTypes.string,
    //TODO: generate these from timestamp or date object
    month: PropTypes.string,
    day: PropTypes.string,
};

SmallEventItem.defaultProps = {

};

export default SmallEventItem;