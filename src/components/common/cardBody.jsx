// @flow
import React from 'react';
import PropTypes from 'prop-types';

const CardBody = (props: any) => (
    <div className={'card__body'} style={{ padding: props.padding }}>
        {
            props.children
        }
    </div>
);

CardBody.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    padding: PropTypes.string,
};

CardBody.defaultProps = {
    padding: '35px 55px'
};

export default CardBody;