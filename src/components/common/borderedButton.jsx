// @flow
import React from 'react';
import PropTypes from 'prop-types';

import theme from '../../theme/theme';

const { DODGER_BLUE, WHITE } = theme.colors;

const getButtonStyle = (props: any) => (
    {
        backgroundColor: props.backgroundColor,
        borderWidth: props.borderWidth,
        borderRadius: props.borderRadius,
        borderColor: props.borderColor,
        height: props.height,
        padding: props.padding,
        fontWeight: props.fontWeight
    }
);

const getLabelStyle  = (props: any) => ({
    color: props.labelColor,
    fontSize: props.labelFontSize
});

const renderIcon = (iconSource) => {
    if ( !iconSource ) {
        return null;
    }
    return (
        <div className={'common__bordered-button__icon__wrapper'}>
            <img src={iconSource} className={'common__bordered-button__icon'}/>
        </div>
    )
};

const BorderedButton = (props: any) => {
    return (
        <button
            type={'button'}
            onClick={props.onClick}
            className={'common__bordered-button'}
            style={getButtonStyle(props)}
        >
            {
                renderIcon(props.iconSource)
            }
            <span className={'common__bordered-button__label'} style={getLabelStyle(props)}>
                {
                    props.label
                }
            </span>
        </button>
    )
};

BorderedButton.propTypes = {
    label: PropTypes.string.isRequired,
    labelFontSize: PropTypes.string,
    labelColor: PropTypes.string,
    onClick: PropTypes.func,
    iconSource: PropTypes.string,
    backgroundColor: PropTypes.string,
    borderWidth: PropTypes.string,
    borderRadius: PropTypes.string,
    borderColor: PropTypes.string,
    height: PropTypes.string.isRequired,
    padding: PropTypes.string,
    fontWeight: PropTypes.number,
};

BorderedButton.defaultProps = {
    labelFontSize: '0.875em',
    labelColor: DODGER_BLUE,
    onClick: () => {},
    iconSource: null,
    backgroundColor: WHITE,
    borderColor: DODGER_BLUE,
    height: '42px',
    borderRadius: '21px',
    padding: '0px 52px',
    borderWidth: '1px',
    fontWeight: 400
};

export default BorderedButton;