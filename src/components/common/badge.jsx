// @flow
import React from 'react';
import PropTypes from 'prop-types';

import theme from '../../theme/theme';
const { MANDY } = theme.colors;

const Badge = (props: any) => (
    <div className={'badge'} style={{ backgroundColor: props.backgroundColor }}>
        <span className={'badge__text'}>
            {
                props.badgeCount
            }
        </span>
    </div>
);

Badge.propTypes = {
    backgroundColor: PropTypes.string,
    badgeCount: PropTypes.number,
};

Badge.defaultProps = {
    backgroundColor: MANDY,
    badgeCount: 0
};

export default Badge;