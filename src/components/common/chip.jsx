// @flow
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CloseIcon from '../../assets/icons/close.png';
import theme from '../../theme/theme';

const { DODGER_BLUE, WHITE } = theme.colors;

const onRemoveClick = (props: any) => {
    if ( props.isRemovable ) {
        return false;
    }
    props.onRemoveClick();
    return true;
};

const renderRemoveButton  = (props: any) => {
    if ( !props.isRemovable ) {
        return null;
    }

    return (
        <button className={'common__chip__remove'} onClick={() => { onRemoveClick(props) }}>
            <div className={'common__chip__remove__icon__wrapper'}>
                <img src={CloseIcon} className={'common__chip__remove__icon'}/>
            </div>
        </button>
    )
};

const Chip = (props: any) => (
    <div
        className={ classnames('common__chip', { 'common__chip--removable': props.isRemovable })}
        style={{ backgroundColor: props.backgroundColor }}>
        <span className={'common__chip__label'}>
            {
                props.label
            }
        </span>
        {
            renderRemoveButton(props)
        }
    </div>
);

Chip.propTypes = {
    label: PropTypes.string.isRequired,
    isRemovable: PropTypes.bool,
    onRemoveClick: PropTypes.func,
};

Chip.defaultProps = {
    isRemovable: false,
    onRemoveClick: () => {},
};

export default Chip;