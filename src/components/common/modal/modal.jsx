// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import update from 'immutability-helper';
import classnames from "classnames";

type Props = {
}

type State = {
    showModal: boolean
}

class Modal extends Component<Props, State> {
    modalBackgroundRef: ?HTMLDivElement;
    constructor(...args) {
        super(...args);
        this.state = {
            showModal: true
        };

        this.modalBackgroundRef = null;
    }

    hideModal = () => {
        document.getElementsByTagName('body')[0].style.overflow = '';
        this.setState(previousState => update(previousState, {showModal: {$set: false}}));
    };

    showModal = (event) => {
        document.getElementsByTagName('body')[0].style.overflow = 'hidden';
        this.setState(previousState => update(previousState, {showModal: {$set: true}}));
        if (event) {
            event.stopPropagation();
        }
    };

    render() {
        return (
            <div
                className={
                    classnames(
                        'modal',
                        {
                            'modal--open': this.state.showModal,
                            'modal--closed': !this.state.showModal
                        })
                }
                onClick={this.hideModal}
                ref={(modalBackgroundRef) => {
                    this.modalBackgroundRef = modalBackgroundRef;
                }}
            >

            </div>
        );
    }
}

Modal.propTypes = {};

Modal.defaultProps = {};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);