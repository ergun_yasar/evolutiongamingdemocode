// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "./card";
import CardBody from "./cardBody";
import BasicUserInformationItem from "./basicUserInformationItem";
import NetworkConnectionInformationItem from "./networkConnectionInformationItem";

import MoreHorizIcon from '../../assets/icons/more_horiz.png';
import HelpingHand from '../../assets/icons/helping_hand_light.png';
import GeoFence from '../../assets/icons/geofence_light.png';

const NetworkConnectionItem = (props: any) => {
    return (
        <Card classNames={{'network-connection-item__card': true}}>
            <img alt={''} src={MoreHorizIcon} className={'network-connection-item__more-button'}/>
            <CardBody padding={'0px 16px'}>
                <div className={"network-connection-item"}>
                    <BasicUserInformationItem
                        domain={props.domain}
                        imageSource={props.imageSource}
                        name={props.name}
                        title={props.title}
                    />
                    <div className={'network-connection-item__network-connection-information-item-wrapper'}>
                        <NetworkConnectionInformationItem
                            iconSource={HelpingHand}
                            text={'Presidents Summit 2018'}
                        />
                        <NetworkConnectionInformationItem
                            iconSource={GeoFence}
                            text={'Copenhagen, 12:52 PM'}
                        />
                    </div>
                </div>
            </CardBody>
            <button className={'network-connection-item__button'}>
                <span className={'network-connection-item__button-label'}>
                    view profile
                </span>
            </button>
        </Card>
    );
};

NetworkConnectionItem.propTypes = {
    domain: PropTypes.string,
    imageSource: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
};

NetworkConnectionItem.defaultProps = {};

export default NetworkConnectionItem;