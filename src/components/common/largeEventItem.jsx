// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';

const LargeEventItem = (props: any) => {
    return (
        <div className={'large-event-item'}>
            <img src={props.imageSource} className={'large-event-item__image'}/>
            <div className={'large-event-item__info-container'}>
                <span className={'large-event-item__date'}>
                    {
                        props.date
                    }
                </span>
                <span className={'large-event-item__name'}>
                    {
                        props.name
                    }
                </span>
                <span className={'large-event-item__detail'}>
                    {
                        props.detail
                    }
                </span>
            </div>
        </div>
    )
}

LargeEventItem.propTypes = {
    imageSource: PropTypes.string,
    name: PropTypes.string,
    detail: PropTypes.string,
    date: PropTypes.string,
};

LargeEventItem.defaultProps = {};

export default LargeEventItem