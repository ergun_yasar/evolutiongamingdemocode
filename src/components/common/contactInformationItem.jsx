// @flow
import React from 'react';
import PropTypes from 'prop-types';

const ContactInformationItem = (props: any) => {
    return (
        <div className={'common__contact-information-item'}>
            <img src={props.iconSource} className={'common__contact-information-item__icon'}/>
            <span className={'common__contact-information-item__label'}>
                {
                    props.label
                }
            </span>
        </div>
    )
};

ContactInformationItem.propTypes = {
    iconSource: PropTypes.string,
    label: PropTypes.string,
};

export default ContactInformationItem;