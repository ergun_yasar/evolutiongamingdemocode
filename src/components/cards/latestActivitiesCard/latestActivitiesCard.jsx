// @flow
import React from 'react';
import PropTypes from 'prop-types';
import TrophyWhiteIcon from '../../../assets/icons/trophy_white.png';
import HelpingHandWhiteIcon from '../../../assets/icons/helpinghand_white.png';
import UserGropuWhiteIcon from '../../../assets/icons/usergroup_white.png';

import INGLogo from '../../../assets/images/exhibitors/ing.png';
import EYLogo from '../../../assets/images/exhibitors/ey.png';
import someCompanyONLogo from '../../../assets/images/exhibitors/someCompanyon.png';
import DeloitteLogo from '../../../assets/images/exhibitors/deloitte.png';
import TMobileLogo from '../../../assets/images/exhibitors/tmobile.png';

import Card from "../../common/card";
import CardHeader from "../../common/cardHeader";
import CardDivider from "../../common/cardDivider";
import CardBody from "../../common/cardBody";
import HighlightItem from "../../common/highlightItem";
import ExhibitorItem from "../../common/exhibitorItem";

import theme from '../../../theme/theme';
const { DUST_GRAY, MIRAGE, DODGER_BLUE, TURQUOISE, LEMON } = theme.colors;





const LatestActivitiesCard = (props: any) => {
    return (
        <Card style={{ flex: 'none' }}>
            <CardHeader title={'ING Week van de Ondernemers'} rightLabel={'April 26, 2018'}/>
            <CardDivider />
            <CardBody padding={'0px 55px 5px'}>
                <div className={'latest-activities-card'}>
                    <span className={'latest-activities-card__section-title'}>
                        Highlights
                    </span>
                    <div className={'latest-activities-card__section-container'}>
                        <HighlightItem
                            backgroundColor={DODGER_BLUE}
                            iconSource={HelpingHandWhiteIcon}
                            label={'You have someCompanyn hands with '}
                            strongLabel={'27 people'}
                        />
                        <HighlightItem
                            backgroundColor={TURQUOISE}
                            iconSource={UserGropuWhiteIcon}
                            label={'You have matched with '}
                            strongLabel={'8 new people'}
                        />
                        <HighlightItem
                            backgroundColor={LEMON}
                            iconSource={TrophyWhiteIcon}
                            label={'You have earned '}
                            strongLabel={'1 new trophy'}
                        />
                    </div>
                    <span className={'latest-activities-card__section-title'}>
                        Exhibitors you showed interest in
                    </span>
                    <div className={'latest-activities-card__section-container'}>
                        <ExhibitorItem
                            imageSource={INGLogo}
                            subTitle={'Suistanable Economy'}
                            title={'ING Week van de Ondernemers'}
                        />
                        <ExhibitorItem
                            imageSource={EYLogo}
                            subTitle={'Finance Navigator'}
                            title={'EY'}
                        />
                        <ExhibitorItem
                            imageSource={someCompanyONLogo}
                            subTitle={'someCompany your way up'}
                            title={'someCompany'}
                        />
                        <ExhibitorItem
                            imageSource={DeloitteLogo}
                            subTitle={'International Expansion'}
                            title={'Deloitte'}
                        />
                    </div>
                    <span className={'latest-activities-card__section-title'}>
                        Other interesting exhibitors on this event
                    </span>
                    <div className={'latest-activities-card__section-container'}>
                        <ExhibitorItem
                            imageSource={TMobileLogo}
                            subTitle={'IoT Platform'}
                            title={'T-Mobile'}
                        />
                    </div>
                </div>
            </CardBody>
        </Card>
    )
};

LatestActivitiesCard.propTypes = {

};

LatestActivitiesCard.defaultProps = {

};

export default LatestActivitiesCard;