// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "../../common/card";
import SmallEventItem from "../../common/smallEventItem";

const InterestingEventsCard = (props: any) => {
    return (
        <div className={'interesting-events-card'}>
            <Card>
                <div className={'interesting-events-card__body'}>
                    <span className={'interesting-events-card__title'}>
                        Interesting events
                    </span>
                    <div className={'interesting-events-card__small-event-item-wrapper'}>
                        <SmallEventItem
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'TodaysArt'}
                            detail={'21 - 23 Sep The Hague'}
                            month={'SEP'}
                            day={'21'}
                        />
                        <SmallEventItem
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F49157264%2F205598759004%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C2%2C700%2C350&s=097401e44ce2239005e1f78bfa5b0266'}
                            name={'Anniversary Weekender'}
                            detail={'6 - 7 Jul\n' +
                            'Amsterdam'}
                            month={'JUL'}
                            day={'6'}
                        />
                        <SmallEventItem
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50508011%2F250719767503%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C28%2C1600%2C800&s=b30bb7f7b7a9879e1db6620e5458bbd4'}
                            name={'Summer Series/Bret'}
                            detail={'4 Aug\n' +
                            'Amsterdam'}
                            month={'AUG'}
                            day={'6'}
                        />
                        <SmallEventItem
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F49636058%2F271338618157%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C0%2C1600%2C800&s=daf0f75500de50886aa7dbdd962ff464'}
                            name={'TodaysArt'}
                            detail={'21 - 23 Sep The Hague'}
                            month={'SEP'}
                            day={'21'}
                        />
                    </div>
                    <span className={'interesting-events-card__more'}>
                        See more
                    </span>
                </div>
            </Card>
        </div>
    )
};

InterestingEventsCard.propTypes = {

};

InterestingEventsCard.defaultProps = {

};

export default InterestingEventsCard;