// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "../../common/card";
import CardBody from "../../common/cardBody";
import CardDivider from "../../common/cardDivider";
import SocialMediaButton from "../../common/socialMediaButton";
import theme from "../../../theme/theme";
import FacebookIcon from '../../../assets/icons/facebook.png';
import LinkedIn from '../../../assets/icons/linkedin.png';
import { faLinkedinIn, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import TrophyIcon from '../../../assets/icons/trophy.png';
import HelpingHandIcon from '../../../assets/icons/helpingHand.png';
import CalendarIcon from '../../../assets/icons/calendar.png';
import BorderedButton from "../../common/borderedButton";
import InformationItem from "../../common/informationItem";

const {TORY_BLUE, DENIM, SAN_MARINO, ROYAL_BLUE, DUST_GRAY, MIRAGE } = theme.colors;

const SocialConnectCard = (props: any) => {
    return (
        <div className={'social-connect-card'}>
            <Card>
                <div className={'social-connect-card__title__container'}>
                    <div className={'social-connect-card__section-wrapper'}>
                        <span className={'social-connect-card__title'}>
                            Connect profile
                        </span>
                        <div className={'social-connect-card__social-media-button__wrapper'}>
                            <SocialMediaButton iconSource={faLinkedinIn} iconBackgroundColor={TORY_BLUE} label={'Connect'} labelBackgroundColor={DENIM}/>
                            <SocialMediaButton iconSource={faFacebookF} iconBackgroundColor={SAN_MARINO} label={'Connect'} labelBackgroundColor={ROYAL_BLUE}/>
                        </div>
                    </div>
                </div>
                <CardDivider />
                <div className={'social-connect-card__body'}>
                    <div className={'social-connect-card__section-wrapper'}>
                        {/*<div className={'social-connect-card__connect-company__wrapper'}>*/}
                            {/*<div className={'social-connect-card__connect-company'}>*/}
                                {/*<BorderedButton*/}
                                    {/*label={'Connect Company'}*/}
                                    {/*height={'54px'}*/}
                                    {/*borderRadius={'27px'}*/}
                                    {/*borderColor={DUST_GRAY}*/}
                                    {/*labelColor={MIRAGE}*/}
                                    {/*labelFontSize={'1.25em'}*/}
                                    {/*fontWeight={500}*/}
                                    {/*padding={'0px 32px'}*/}
                                {/*/>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                        <div className={'social-connect-card__information-item-container'}>
                            {/*<InformationItem iconSource={TrophyIcon} label={'You have '} strongLabel={'8 trophies'}/>*/}
                            <InformationItem iconSource={HelpingHandIcon} label={'You have '} strongLabel={'89 connections'}/>
                            <InformationItem iconSource={CalendarIcon} label={'You have visited '} strongLabel={'8 events'}/>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
    )
};

SocialConnectCard.propTypes = {

};

SocialConnectCard.defaultProps = {

};

export default SocialConnectCard;