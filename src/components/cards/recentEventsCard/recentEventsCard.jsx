// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "../../common/card";
import CardHeader from "../../common/cardHeader";
import CardDivider from "../../common/cardDivider";
import CardBody from "../../common/cardBody";
import MediumEventItem from "../../common/mediumEventItem";

const RecentEventsCard = (props: any) => {
    return (
        <Card>
            <CardHeader title={'Recent Events'}/>
            <CardDivider/>
            <CardBody>
                <div className={'recent-events-card'}>
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                    <MediumEventItem
                        date={'Thu, Oct 11, 8:30am'}
                        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
                        name={'Summer Series/Bret 4 Aug Amsterdam'}
                    />
                </div>
            </CardBody>
        </Card>
    )
};

RecentEventsCard.propTypes = {

};

RecentEventsCard.defaultProps =  {

};

export default RecentEventsCard;