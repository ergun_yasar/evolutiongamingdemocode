// @flow
import React from 'react';
import PropTypes from 'prop-types';
import RoundImage from "../../common/roundImage";
import Card from "../../common/card";
import EditIcon from '../../../assets/icons/edit.png';
import MoreHorizIcon from '../../../assets/icons/more_horiz.png';

const ProfileInformationCard = (props: any) => {
    return (
        <Card>
            <div className={'profile-information-card__info'}>
                <RoundImage width={'184px'} height={'184px'} src={props.profileImage}/>
                <span className={'profile-information-card__info__text profile-information-card__info__text--bold'} style={{ paddingBottom: '-10px' }}>
                    {
                        props.name
                    }
                </span>
                <span className={'profile-information-card__info__text'}>
                    {
                        props.title
                    }
                    &nbsp;
                    <span className={'profile-information-card__info__text profile-information-card__info__text--medium'}>
                    {
                        props.domain
                    }
                    </span>
                </span>

                <img src={MoreHorizIcon} className={'profile-information-card__info__more-button'}/>
            </div>
            <div className={'card__divider'}/>
            <div className={'profile-information-card__slogan'}>
                <span className={'profile-information-card__slogan__text'}>
                    {
                        props.slogan
                    }
                </span>
                <img src={EditIcon} className={'profile-information-card__slogan__edit-button'}/>
            </div>
        </Card>
    );
};

ProfileInformationCard.propTypes = {
    profileImage: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
    domain: PropTypes.string,
    slogan: PropTypes.string,
};

ProfileInformationCard.defaultProps = {
    profileImage: '',
    name: '',
    description: '',
    domain: '',
};

export default ProfileInformationCard;