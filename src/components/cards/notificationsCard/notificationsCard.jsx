// @flow
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Card from "../../common/card";
import CardBody from "../../common/cardBody";
import Badge from "../../common/badge";
import NotificationItem from "../../common/notificationItem";

const NotificationsCard = (props: any) => {
    return (
        <Card style={{ position: 'relative', flex: 'none' }} classNames={props.classNames}>
            <CardBody padding={'5px 20px 52px'}>
                <div className={'notifications-card'}>
                    <div className={'notifications-card__title-container'}>
                        <span className={'notifications-card__title'}>
                            Notifications
                        </span>
                        <Badge badgeCount={3}/>
                    </div>
                    <NotificationItem
                        imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
                        title={'Babak Heydari'}
                        text={'requested to see your contact details'}
                    />
                    <NotificationItem
                        imageSource={'https://randomuser.me/api/portraits/men/8.jpg'}
                        title={'Menno van der Reek'}
                        text={'requested to see your contact details'}
                    />
                </div>
            </CardBody>
            <button className={'notifications-card__button'} onClick={props.onFooterClick}>
                <span className={'notifications-card__button-label'}>
                    Manage all
                </span>
            </button>
        </Card>
    )
};

NotificationsCard.propTypes = {
    badgeCount: PropTypes.number,
    classNames: PropTypes.object,
    onFooterClick: PropTypes.func,
};

NotificationsCard.defaultProps = {};

export default NotificationsCard;