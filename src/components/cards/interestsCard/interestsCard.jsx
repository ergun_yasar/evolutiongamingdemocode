// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "../../common/card";
import CardHeader from "../../common/cardHeader";
import CardDivider from "../../common/cardDivider";
import CardBody from "../../common/cardBody";
import Chip from "../../common/chip";
import BorderedButton from "../../common/borderedButton";

const InterestsCard = (props: any) => {
    return (
        <Card>
            <CardHeader title={'Interests'}/>
            <CardDivider/>
            <CardBody>
                <div className={'interests-card__container'}>
                    <span className={'interests-card__explanation'}>
                        Add your interests to match with other attendees on events you visit.
                    </span>
                    <div className={'interests-card__chip-container'}>
                        <Chip label={'Oil & gas'} isRemovable={true}/>
                        <Chip label={'Automotive'} isRemovable={true}/>
                        <Chip label={'IoT'} isRemovable={true}/>
                        <Chip label={'Blockchain'} isRemovable={true}/>
                        <Chip label={'Automotive'} isRemovable={true}/>
                        <Chip label={'IoT'} isRemovable={true}/>
                        <BorderedButton label={'Add Interest'} height={'38px'} labelFontSize={'1.125em'} borderWidth={'2.1px'} padding={'0 40px'}/>
                    </div>
                </div>
            </CardBody>
        </Card>
    )
};

InterestsCard.propTypes = {};

InterestsCard.defaultProps = {};

export default InterestsCard;