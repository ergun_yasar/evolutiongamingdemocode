// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';

import theme from '../../../theme/theme';
import { faLinkedinIn, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import Card from '../../common/card';
import SocialMediaButton from '../../common/socialMediaButton';
import Chip from '../../common/chip';

const { DUST_GRAY, MIRAGE, TORY_BLUE, DENIM, SAN_MARINO, ROYAL_BLUE } = theme.colors;

const MatchCard = (props: any) => {
    return (
        <Card style={{ padding: 0, overflow: 'hidden' }}>
            <div className={'match-card__header-container'}>
                <span className={'match-card__header'}>
                    Attending ING week van Ondernemers
                </span>
            </div>
            <div className={'match-card__info-container'}>
                <img src={props.imageSource} className={'match-card__image'}/>
                <span className={'match-card__name'}>
                    {
                        props.name
                    }
                </span>
                <span className={'match-card__title'}>
                    {
                        `${props.title} at `
                    }
                    <br />
                    {
                        props.domain
                    }
                </span>
                <SocialMediaButton iconSource={faLinkedinIn} iconBackgroundColor={TORY_BLUE} label={'Profile'} labelBackgroundColor={DENIM} style={{ marginTop: '22px' }}/>
            </div>
            <div className={'match-card__slogan-container'}>
                <span className={'match-card__slogan'}>
                    {
                        `“ ${props.slogan}“`
                    }
                </span>
            </div>
            <div className={'match-card__interests-container'}>
                <span className={'match-card__interests-title'}>
                    Interests
                </span>
                <div className={'match-card__interests-wrapper'}>
                    <Chip label={'Oil & gas'} />
                    <Chip label={'Automotive'} />
                    <Chip label={'IoT'} />
                    <Chip label={'Blockchain'} />
                    <Chip label={'Automotive'}/>
                    <Chip label={'IoT'} />
                </div>
            </div>
            <div className={'match-card__buttons-container'}>
                <button className={'match-card__button'}>
                    <span className={'match-card__button-label'}>
                        SKIP
                    </span>
                </button>
                <button className={'match-card__button match-card__button--active'}>
                    <span className={'match-card__button-label match-card__button-label--active'}>
                        INTERESTED
                    </span>
                </button>
            </div>
        </Card>
    )
};

MatchCard.propTypes = {
    domain: PropTypes.string,
    imageSource: PropTypes.string,
    name: PropTypes.string,
    slogan: PropTypes.string,
    title: PropTypes.string,
};

MatchCard.defaultProps = {};

export default MatchCard