// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';

import Card from "../../common/card";

//TODO: Actual star icon will be added
import StarIcon from '../../../assets/icons/edit.png';
import ProgressBar from "../../common/progressBar";
import ContactInformationItem from "../../common/contactInformationItem";

import SuitcaseIcon from '../../../assets/icons/suitcase.png';
import BuildingIcon from '../../../assets/icons/building.png';
import ArrowDownIcon from '../../../assets/icons/arrow_down_blue.png';

import BorderedButton from "../../common/borderedButton";

const ProfileStrengthCard = (props: any) => {
    return (
        <Card>
            <div className={'profile-strength-card'}>
                <div className={'profile-strength-card__strength-info'}>
                    <div className={'profile-strength-card__strength-info__text__container'}>
                        <span className={'profile-strength-card__strength-info__text'}>
                            {
                                `Profile strength: `
                            }
                            <span className={'profile-strength-card__strength-info__text--strong'}>
                                {
                                    props.strengthLevel
                                }
                                <FontAwesomeIcon icon={faStar} className={'profile-strength-card__strength-info__icon'} />
                            </span>
                        </span>

                    </div>
                    <span className={'profile-strength-card__strength-info__counter'}>
                        {
                            `(${props.currentStep}/${props.totalSteps})`
                        }
                    </span>
                </div>
                <ProgressBar currentStep={props.currentStep} totalSteps={props.totalSteps}/>
                <span className={'profile-strength-card__title'}>
                    {
                        `Strengthen your profile`
                    }
                </span>
                <span className={'profile-strength-card__explanation'}>
                    {
                        `You can make the most out of your networking by providing additional information for your contacts. Only select information is shared with handsomeCompany contacts, any additional information is  exclusively shared with those you accept into your Network.`
                    }
                </span>
                <ContactInformationItem iconSource={SuitcaseIcon} label={props.title}/>
                <ContactInformationItem iconSource={BuildingIcon} label={props.domain}/>
                <ContactInformationItem iconSource={SuitcaseIcon} label={props.phoneNumber}/>
                <ContactInformationItem iconSource={BuildingIcon} label={props.emailAddress}/>
                <div className={'profile-strength-card__update-profile'}>
                    <BorderedButton label={'Update Profile'} height={'42px'} borderRadius={'11px'} iconSource={ArrowDownIcon} fontWeight={700}/>
                </div>
            </div>
        </Card>
    )
};

ProfileStrengthCard.propTypes = {
    strengthLevel: PropTypes.string,
    currentStep: PropTypes.number,
    totalSteps: PropTypes.number,
    title: PropTypes.string,
    domain: PropTypes.string,
    phoneNumber: PropTypes.string,
    emailAddress: PropTypes.string,
};

ProfileStrengthCard.defaultProps = {
    strengthLevel: 'All-star profile'
};

export default ProfileStrengthCard;