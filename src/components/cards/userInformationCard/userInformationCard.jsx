// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Card from "../../common/card";
import CardBody from "../../common/cardBody";
import BasicUserInformationItem from "../../common/basicUserInformationItem";
import InformationItem from "../../common/informationItem";

import TrophyIcon from '../../../assets/icons/trophy.png';
import HelpingHandIcon from '../../../assets/icons/helpingHand.png';
import CalendarIcon from '../../../assets/icons/calendar.png';
import Chip from '../../common/chip';

const UserInformationCard = (props: any) => {
    return (
        <Card style={{ flex: 'none' }}>
            <CardBody padding={'20px 20px 35px 20px'}>
                <div className={'user-information-item'}>
                    <BasicUserInformationItem
                        domain={props.domain}
                        imageSource={props.imageSource}
                        name={props.name}
                        title={props.title}
                    />
                    {
                        props.showStatistics && (
                            <div className={'user-information-item__information-item-wrapper'}>
                                <InformationItem iconSource={TrophyIcon} label={'You have '} strongLabel={'8 trophies'}/>
                                <InformationItem iconSource={HelpingHandIcon} label={'You have '} strongLabel={'89 connections'}/>
                                <InformationItem iconSource={CalendarIcon} label={'You have visited '} strongLabel={'8 events'}/>
                            </div>
                        )
                    }
                    {
                        props.showSlogan && (
                            <div className={'user-information-card__slogan-container'}>
                                <span className={'user-information-card__slogan'}>
                                    {
                                        props.slogan
                                    }
                                </span>
                            </div>
                        )
                    }
                    {
                        props.showInterests && (
                            <div className={'user-information-card__interests-container'}>
                                <span className={'user-information-card__interests-title'}>
                                    Interests
                                </span>
                                <div className={'user-information-card__interests-wrapper'}>
                                    <Chip label={'Oil & gas'} />
                                    <Chip label={'Automotive'} />
                                    <Chip label={'IoT'} />
                                    <Chip label={'Blockchain'} />
                                    <Chip label={'Automotive'}/>
                                    <Chip label={'IoT'} />
                                </div>
                            </div>
                        )
                    }
                </div>
            </CardBody>
        </Card>
    )
};

UserInformationCard.propTypes = {
    domain: PropTypes.string,
    imageSource: PropTypes.string,
    name: PropTypes.string,
    showInterests: PropTypes.bool,
    showSlogan: PropTypes.bool,
    showStatistics: PropTypes.bool,
    slogan: PropTypes.string,
    title: PropTypes.string,
};

UserInformationCard.defaultProps = {
    showInterests: false,
    showSlogan: false,
    showStatistics: true
};

export default UserInformationCard;