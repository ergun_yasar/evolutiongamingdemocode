// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import type { Dispatch } from 'redux';
import update from 'immutability-helper';
import InterestingEventsCard from "../components/cards/interestingEventsCard/interestingEventsCard";
import UserInformationCard from "../components/cards/userInformationCard/userInformationCard";
import LatestActivitiesCard from "../components/cards/latestActivitiesCard/latestActivitiesCard";
import ScreenHeader from '../components/common/screenHeader';

class Home extends Component<any> {
    constructor(...args) {
        super(...args);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className={'home__container'}>
                <ScreenHeader>
                    <div className={'tickets__header-container'}>
                        <div className={'tickets__header-wrapper'}>
                            <span className={'tickets__header-title'}>
                                Your event journey
                            </span>
                            <span className={'tickets__header-subtitle'}>
                                see  yourlatest activities on event(s)
                            </span>
                        </div>
                    </div>
                </ScreenHeader>
                <div className={'home__user-information-card-container'}>
                    <UserInformationCard
                        domain={'someCompany'}
                        imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
                        name={'Babak Heydari'}
                        title={'CEO & Founder'}
                    />
                </div>
                <div className={'home__latest-activities-card-container'}>
                    <LatestActivitiesCard/>
                </div>
                <div className={'home__interesting-events-card-container'}>
                    <InterestingEventsCard/>
                </div>
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);