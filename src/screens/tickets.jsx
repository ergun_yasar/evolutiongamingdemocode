// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type {Dispatch} from 'redux';
import update from 'immutability-helper';
import classnames from 'classnames';

import InterestingEventsCard from "../components/cards/interestingEventsCard/interestingEventsCard";
import ScreenHeader from "../components/common/screenHeader";
import LargeEventItem from "../components/common/largeEventItem";

type Props = {
};

type State = {
    selectedTabIndex: number
}

class Tickets extends Component<Props, State> {
    static defaultProps: any;
    constructor(...args) {
        super(...args);
        this.state = {
            selectedTabIndex: 0
        }
    }

    isTabActive = (tabIndex) => tabIndex === this.state.selectedTabIndex;

    onTabClick = (tabIndex) => {
        this.setState(previousState => update(previousState, {selectedTabIndex: {$set: tabIndex}}));
    };

    render() {
        return (
            <div className={'tickets__container'}>
                <ScreenHeader>
                    <div className={'tickets__header-container'}>
                        <div className={'tickets__header-wrapper'}>
                            <span className={'tickets__header-title'}>
                                Tickets
                            </span>
                            <span className={'tickets__header-subtitle'}>
                                Your event tickets
                            </span>
                        </div>
                    </div>
                </ScreenHeader>
                <div className={'tickets__left-container'}>
                    <div className={'tickets__tabs-container'}>
                        <div className={'tickets__tabs-wrapper'} onClick={() => this.onTabClick(0)}>
                            <span className={'tickets__tabs-title'}>
                                Upcoming Events  (5)
                            </span>
                            <div className={classnames('tickets__tabs-bottom', { 'tickets__tabs-bottom--active': this.isTabActive(0) })}/>
                        </div>
                        <div className={'tickets__tabs-wrapper'} onClick={() => this.onTabClick(1)}>
                            <span className={'tickets__tabs-title'}>
                                Past Events  (10)
                            </span>
                            <div className={classnames('tickets__tabs-bottom', { 'tickets__tabs-bottom--active': this.isTabActive(1) })}/>
                        </div>
                        <div className={'tickets__tabs-wrapper'} onClick={() => this.onTabClick(2)}>
                            <span className={'tickets__tabs-title'}>
                                Saved Events  (3)
                            </span>
                            <div className={classnames('tickets__tabs-bottom', { 'tickets__tabs-bottom--active': this.isTabActive(2) })}/>
                        </div>
                    </div>
                    <div className={classnames('tickets__events-container', { 'tickets__events-container--hidden': !this.isTabActive(0) })}>
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                    </div>
                    <div className={classnames('tickets__events-container', { 'tickets__events-container--hidden': !this.isTabActive(1) })}>
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                    </div>
                    <div className={classnames('tickets__events-container', { 'tickets__events-container--hidden': !this.isTabActive(2) })}>
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                        <LargeEventItem
                            date={'NOVEMBER 15, 2017 - 8:00 AM'}
                            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
                            name={'Summer Series/Bret 4 Aug Amsterdam'}
                            detail={'Free order #695935231 on November 9, 2017'}
                        />
                    </div>
                </div>
                <div className={'tickets__right-container'}>
                    <InterestingEventsCard/>
                </div>
            </div>
        );
    }
}

Tickets.propTypes = {};

Tickets.defaultProps = {};

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Tickets)