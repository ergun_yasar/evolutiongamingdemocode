// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import type { Dispatch } from 'redux';
import update from 'immutability-helper';
import ProfileInformationCard from "../components/cards/profileInformationCard/profileInformationCard";
import ProfileStrengthCard from "../components/cards/profileStrengthCard/profileStrengthCard";
import InterestsCard from "../components/cards/interestsCard/interestsCard";
import RecentEventsCard from "../components/cards/recentEventsCard/recentEventsCard";
import SocialConnectCard from "../components/cards/socialConnectCard/socialConnectCard";
import InterestingEventsCard from "../components/cards/interestingEventsCard/interestingEventsCard";

class Profile extends Component<any> {
    constructor(...args) {
        super(...args);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className={'profile__container'}>
                <div className={'profile__left-container'}>
                    <ProfileInformationCard
                        profileImage={'https://randomuser.me/api/portraits/men/9.jpg'}
                        name={'Babak Heydari,'}
                        title={'CEO & Founder at someCompany'}
                        slogan={'“Looking for interesting partners in the event industry”'}/>
                    <div className={'profile__social-connect-card-container'}>
                        <SocialConnectCard/>
                    </div>
                    <div className={'profile__profile-strength-card-container'}>
                        <ProfileStrengthCard
                            strengthLevel={'All-star profile'}
                            currentStep={3}
                            totalSteps={5}
                            title={'CEO  & Founder'}
                            domain={'someCompany'}
                            phoneNumber={'+31635467290'}
                            emailAddress={'babak@someCompany.com'}
                        />
                    </div>
                    <div className={'profile__interests-card-container'}>
                        <InterestsCard/>
                    </div>
                    <div className={'profile__recent-event-card-container'}>
                        <RecentEventsCard/>
                    </div>
                </div>
                <div className={'profile__right-container'}>
                    <div className={'profile__social-connect-card-container'}>
                        <SocialConnectCard/>
                    </div>
                    <div className={'profile__interesting-events-card-container'}>
                        <InterestingEventsCard/>
                    </div>
                </div>
            </div>
        );
    }
};


const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);