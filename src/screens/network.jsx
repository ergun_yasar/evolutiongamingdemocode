// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type {Dispatch} from "redux"
import update from 'immutability-helper';
import classnames from 'classnames';
import ScreenHeader from "../components/common/screenHeader";
import NotificationsCard from "../components/cards/notificationsCard/notificationsCard";
import NetworkConnectionItem from "../components/common/networkConnectionItem";

import FilterIcon from '../assets/icons/filter.png';
import Card from "../components/common/card";
import CardBody from "../components/common/cardBody";
import Select from "../components/common/select/select";
import BorderedButton from "../components/common/borderedButton";
import Modal from '../components/common/modal/modal';

type Props = {
    history: any
}

type State = {
    showModal: boolean
}

class Network extends Component<Props, State> {
    modalBackgroundRef: ?HTMLDivElement;

    constructor(...args) {
        super(...args);
        this.state = {
            showModal: false
        };

        this.modalBackgroundRef = null;
    }

    hideModal = () => {
        document.getElementsByTagName('body')[0].style.overflow = '';
        this.setState(previousState => update(previousState, {showModal: {$set: false}}));
    };

    showModal = (event) => {
        document.getElementsByTagName('body')[0].style.overflow = 'hidden';
        this.setState(previousState => update(previousState, {showModal: {$set: true}}));
        if (event) {
            event.stopPropagation();
        }
    };

    onFilterClick = () => {
        this.showModal();
    };

    onNotificationsClick = () => {
        this.props.history.push('/requests');
    };

    onSaveClick = (event) => {
        this.hideModal();
        event.stopPropagation();
    };

    render() {
        return (
            <div className={'network__container'}>
                <ScreenHeader/>
                <div className={'network__left-container'}>
                    <div className={'network__header'}>
                        <div className={'network__title-container'}>
                            <span className={'network__title'}>
                                Network
                            </span>
                            <span className={'network__subtitle'}>
                                Your connections
                            </span>
                        </div>
                        <div className={'network__filter-container'} onClick={this.onFilterClick}>
                            <span className={'network__filter-text'}>
                                Filter/Sort
                            </span>
                            <div className={'network__filter-icon-container'}>
                                <img src={FilterIcon} className={'network__filter-icon'}/>
                            </div>
                        </div>
                    </div>
                    <NotificationsCard onFooterClick={this.onNotificationsClick} classNames={{ 'network__notification-card': true }}/>
                    <div className={'network__network-connection-items-container'}>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/men/1.jpg'}
                                name={'Babak Heydari'}
                                title={'CEO & Founder'}
                                domain={'someCompany'}
                            />
                        </div>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/women/1.jpg'}
                                name={'Sabina Van Dijk'}
                                title={'Event Manager'}
                                domain={'ING'}
                            />
                        </div>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/men/2.jpg'}
                                name={'Maarten Freriks'}
                                title={'Directuer'}
                                domain={'ID Plein'}
                            />
                        </div>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/women/2.jpg'}
                                name={'Renate Sportel'}
                                title={'Manager Innovation AH Online'}
                                domain={'Ahold Delhaize'}
                            />
                        </div>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/men/3.jpg'}
                                name={'Menno van der Reek'}
                                title={'Embedded Engineer'}
                                domain={'Toon'}
                            />
                        </div>
                        <div className={'network__network-connection-item-wrapper'}>
                            <NetworkConnectionItem
                                imageSource={'https://randomuser.me/api/portraits/women/3.jpg'}
                                name={'Carmen Jenkins'}
                                title={'CEO'}
                                domain={'Share2U'}
                            />
                        </div>
                    </div>
                </div>
                <div className={'network__right-container'}>
                    <NotificationsCard onFooterClick={this.onNotificationsClick}/>
                </div>
                {/*<Modal/>*/}
                <div
                    className={
                        classnames(
                            'network__modal',
                            {
                                'network__modal--open': this.state.showModal,
                                'network__modal--closed': !this.state.showModal
                            })
                    }
                    onClick={this.hideModal}
                    ref={(modalBackgroundRef) => {
                        this.modalBackgroundRef = modalBackgroundRef;
                    }}
                >
                    <div className={'network__modal-container'}>
                        <Card onClick={this.showModal}>
                            <CardBody padding={'35px 65px 25px'}>
                                <div className={'network__modal-body'}>
                                    <span className={'network__modal-title'}>
                                        Sort
                                    </span>
                                    <Select
                                        options={[{key: '0', label: 'lastest'}]}
                                        showPlaceholderOption={true}
                                    />
                                    <span className={'network__modal-title'}>
                                        Filter
                                    </span>
                                    <Select
                                        options={[{key: '0', label: 'lastest'}]}
                                        placeholder={'All Events'}
                                        showPlaceholderOption={true}
                                    />
                                    <Select
                                        options={[{key: '0', label: 'lastest'}]}
                                        placeholder={'All Connections'}
                                        showPlaceholderOption={true}
                                    />
                                    <div className={'network__modal-button'}>
                                        <BorderedButton
                                            label={'Save'}
                                            height={'35px'}
                                            borderRadius={'10px'}
                                            padding={'0 20px'}
                                            onClick={this.onSaveClick}
                                        />
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Network)