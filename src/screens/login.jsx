// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type{Dispatch} from 'redux';
import update from 'immutability-helper';
import authenticationActionCreator from "../actionCreators/authenticationActionCreator";
import {Redirect} from "react-router-dom";

type Props = {
    authenticationActions: any,
    location: any,
    isAuthenticated: ?boolean,
};

type State = {
    email: string;
    password: string;
}

class Login extends Component<Props, State> {
    static defaultProps: Object;

    constructor(...args: any) {
        super(...args);
        this.state = {
            email: '',
            password: '',
        }
    }

    onEmailChange = (event): void => {
        this.setState({ email: event.target.value })
    };

    onLoginClick = (): void => {
        this.props.authenticationActions.login(this.state.email, this.state.password);
    };

    onPasswordChange = (event): void => {
        this.setState({ password: event.target.value })
    };

    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { isAuthenticated } = this.props;

        if (isAuthenticated) {
            return <Redirect to={from} />
        }

        return (
            <div style={{ paddingTop: '10px', maxWidth: '300px', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <input type={'text'} placeholder={'Email'} style={{
                    marginTop: '20px',
                    display: 'block',
                    fontFamily: 'GalanoClassic',
                    width: '100%',
                    height: '28px',
                    fontWeight: '500',
                    padding: '6px 12px 5px',
                    fontSize: '1.25em',
                    lineHeight: '1.42857143',
                    color: '#555',
                    backgroundColor: '#fff',
                    backgroundImage: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '10px',
                    boxShadow: 'inset 0 1px 1px rgba(0,0,0,.075)',
                    transition: 'border-color ease-in-out .15s,box-shadow ease-in-out .15s',
                }} value={this.state.email} onChange={this.onEmailChange}/>
                <input type={'password'} placeholder={'Password'} style={{
                    marginTop: '20px',
                    display: 'block',
                    fontFamily: 'GalanoClassic',
                    width: '100%',
                    height: '28px',
                    fontWeight: '500',
                    padding: '6px 12px 5px',
                    fontSize: '1.25em',
                    lineHeight: '1.42857143',
                    color: '#555',
                    backgroundColor: '#fff',
                    backgroundImage: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '10px',
                    boxShadow: 'inset 0 1px 1px rgba(0,0,0,.075)',
                    transition: 'border-color ease-in-out .15s,box-shadow ease-in-out .15s',
                }} value={this.state.password} onChange={this.onPasswordChange}/>
                <button type={'button'} style={{
                    marginTop: '20px',
                    display: 'block',
                    fontFamily: 'GalanoClassic',
                    width: '100%',
                    height: '56px',
                    fontWeight: '500',
                    padding: '6px 12px 5px',
                    fontSize: '1.25em',
                    lineHeight: '1.42857143',
                    backgroundColor: '#576CE4',
                    color: '#FFF',
                    border: '1px solid #ccc',
                    borderRadius: '10px',
                    boxShadow: 'inset 0 1px 1px rgba(0,0,0,.075)',
                    transition: 'border-color ease-in-out .15s,box-shadow ease-in-out .15s',
                }} onClick={this.onLoginClick}>
                    Login
                </button>
            </div>
        );
    }
}

Login.propTypes = {};

Login.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.authentication.isAuthenticated,
    }
};

const mapDispatchToProps = (dispatch:Dispatch<*>): any => {
    return {
        authenticationActions: bindActionCreators(authenticationActionCreator, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login)