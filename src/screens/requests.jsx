// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import type {Dispatch} from 'redux';
import update from 'immutability-helper';
import classnames from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft, faInbox, faPaperPlane} from '@fortawesome/free-solid-svg-icons';
import ScreenHeader from "../components/common/screenHeader";
import Alert from "../components/common/alert";
import Card from "../components/common/card";
import Badge from "../components/common/badge";
import theme from '../theme/theme';
import NotificationItem from "../components/common/notificationItem";

const {ROYAL_BLUE_LIGHT, STORM_GRAY} = theme.colors;

type Props = {
    history: any
}

type State = {
    selectedTabIndex: number
}

class Requests extends Component<Props, State> {
    static defaultProps: any;
    constructor(...args) {
        super(...args);
        this.state = {
            selectedTabIndex: 0
        }
    }

    isTabActive = (tabIndex) => tabIndex === this.state.selectedTabIndex;

    onTabClick = (tabIndex) => {
        this.setState(previousState => update(previousState, {selectedTabIndex: {$set: tabIndex}}));
    };

    renderBackNavigation = () => {
        if (this.props.history.length <= 1) {
            return null;
        }

        return (
            <div className={'requests__back-container'}>
                <div
                    onClick={() => {
                        this.props.history.goBack();
                    }} className={'requests__back-wrapper'}
                >
                    <FontAwesomeIcon icon={faArrowLeft} className={'requests__back-icon'}/>
                    <span className={'requests__back-title'}>
                        Requests
                    </span>
                </div>
            </div>
        )
    };

    render() {
        return (
            <div className={'requests__container'}>
                <ScreenHeader>
                    {
                        this.renderBackNavigation()
                    }
                </ScreenHeader>
                <div className={'requests__content-wrapper'}>

                    <Alert
                        imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
                        label={'accepted your request'}
                        strongLabel={'Babak Heydari'}
                    />
                    <Card classNames={{'requests__tabs-container': true}}>
                        <div className={'requests__tabs'}>
                            <button
                                className={
                                    classnames(
                                        'requests__tab',
                                        {
                                            'requests__tab--active': this.isTabActive(0),
                                            'requests__tab--inactive': !this.isTabActive(0)
                                        }
                                    )
                                }
                                onClick={() => {
                                    this.onTabClick(0)
                                }}
                            >
                                <FontAwesomeIcon icon={faInbox} className={'requests__tab-icon'}/>
                                <span className={'requests__tab-title'}>
                                    Received
                                </span>
                                <Badge
                                    badgeCount={3}
                                    backgroundColor={this.isTabActive(0) ? ROYAL_BLUE_LIGHT : STORM_GRAY}
                                />
                            </button>
                            <button
                                className={
                                    classnames(
                                        'requests__tab',
                                        {
                                            'requests__tab--active': this.isTabActive(1),
                                            'requests__tab--inactive': !this.isTabActive(1)
                                        }
                                    )
                                }
                                onClick={() => {
                                    this.onTabClick(1)
                                }}
                            >
                                <FontAwesomeIcon icon={faPaperPlane} className={'requests__tab-icon'}/>
                                <span className={'requests__tab-title'}>
                                    Sent
                                </span>
                                <Badge
                                    badgeCount={3}
                                    backgroundColor={this.isTabActive(1) ? ROYAL_BLUE_LIGHT : STORM_GRAY}
                                />
                            </button>
                        </div>
                        <div
                            className={classnames('requests__tab-panel', {'requests__tab-panel--hidden': !this.isTabActive(0)})}
                        >
                            <NotificationItem
                                imageSource={'https://randomuser.me/api/portraits/men/5.jpg'}
                                title={'Babak Heydari'}
                                text={'CEO at someCompany'}
                                showHorizontal={true}
                            />
                            <NotificationItem
                                imageSource={'https://randomuser.me/api/portraits/men/6.jpg'}
                                title={'Babak Heydari'}
                                text={'CEO at someCompany'}
                                showHorizontal={true}
                            />
                        </div>
                        <div
                            className={classnames('requests__tab-panel', {'requests__tab-panel--hidden': !this.isTabActive(1)})}
                        >
                            <NotificationItem
                                imageSource={'https://randomuser.me/api/portraits/men/5.jpg'}
                                title={'Babak Heydari'}
                                text={'CEO at someCompany On'}
                                showHorizontal={true}
                            />
                        </div>
                    </Card>
                </div>
            </div>
        );
    }
}

Requests.propTypes = {};

Requests.defaultProps = {};

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch:Dispatch<*>) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Requests)