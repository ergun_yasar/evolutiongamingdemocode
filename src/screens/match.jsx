// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import update from 'immutability-helper';
import UserInformationCard from '../components/cards/userInformationCard/userInformationCard';
import LatestActivitiesCard from '../components/cards/latestActivitiesCard/latestActivitiesCard';
import InterestingEventsCard from '../components/cards/interestingEventsCard/interestingEventsCard';
import MatchCard from '../components/cards/matchCard/matchCard';
import Card from '../components/common/card';
import CardBody from '../components/common/cardBody';
import ScreenHeader from '../components/common/screenHeader';

class Match extends Component
{
    constructor(...args)
    {
        super(...args);
    }

    render()
    {
        return (
            <div style={{
                display: 'flex',
                flex: 1,
                maxWidth: '1440px',
                justifyContent: 'space-between',
                padding: '60px 80px 40px'
            }}>
                <ScreenHeader/>
                <div style={{ display: 'flex', flexDirection: 'column', marginRight: '22px', zIndex: 10, maxWidth: '250px' }}>
                    <UserInformationCard
                        domain={'someCompany'}
                        imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
                        name={'Babak Heydari'}
                        showInterests={true}
                        showStatistics={false}
                        showSlogan={true}
                        slogan={'“ Looking for interesting IoT companies”'}
                        title={'CEO & Founder'}

                    />
                </div>
                <div style={{
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'column',
                    marginLeft: '22px',
                    marginRight: '22px',
                    zIndex: 10,
                    position: 'relative'
                }}>
                    <MatchCard
                        imageSource={'https://randomuser.me/api/portraits/men/4.jpg'}
                        name={'Menno van der Reek'}
                        title={'CEO & Founder'}
                        domain={'someCompany'}
                        slogan={'Looking for interesting partners in the event industry'}
                    />
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '22px', zIndex: 10, maxWidth: '360px' }}>
                    <Card style={{ flex: 'none' }}>
                        <CardBody padding={'10px 25px 20px'}>
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <span style={{
                                    color: '#1B2235',
                                    fontFamily: 'GalanoClassic',
                                    fontWeight: '700',
                                    fontSize: '1.25em',
                                    paddingTop: '0.3em',
                                }}>
                                    Match & Meet
                                </span>
                                <span style={{
                                    color: '#1B2235',
                                    fontFamily: 'GalanoClassic',
                                    fontSize: '1.25em',
                                    paddingTop: '0.3em',
                                    marginTop: '20px',
                                }}>
                                    Swipe through interesting people and match with the ones you want to meet
                                </span>
                            </div>
                        </CardBody>
                    </Card>
                    <Card style={{ flex: 'none', marginTop: '30px' }}>
                        <CardBody padding={'10px 25px 20px'}>
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <span style={{
                                    color: '#1B2235',
                                    fontFamily: 'GalanoClassic',
                                    fontWeight: '700',
                                    fontSize: '1.25em',
                                    paddingTop: '0.3em',
                                }}>
                                    Select Event
                                </span>
                                <span style={{
                                    color: '#1B2235',
                                    fontFamily: 'GalanoClassic',
                                    fontSize: '1.25em',
                                    paddingTop: '0.3em',
                                    marginTop: '20px',
                                }}>
                                    Please select an event to get an overview of its attendees. If no event is selected you will see an overview of people of all events you are attending
                                </span>
                                <select style={{
                                    marginTop: '20px',
                                    display: 'block',
                                    fontFamily: 'GalanoClassic',
                                    width: '100%',
                                    height: '56px',
                                    fontWeight: '500',
                                    padding: '6px 12px 5px',
                                    fontSize: '1.25em',
                                    lineHeight: '1.42857143',
                                    color: '#555',
                                    backgroundColor: '#fff',
                                    backgroundImage: 'none',
                                    border: '1px solid #ccc',
                                    borderRadius: '10px',
                                    boxShadow: 'inset 0 1px 1px rgba(0,0,0,.075)',
                                    transition: 'border-color ease-in-out .15s,box-shadow ease-in-out .15s',
                                }}>
                                    <option>
                                        ING week van Ondernemers
                                    </option>
                                </select>
                            </div>
                        </CardBody>
                    </Card>
                </div>
            </div>
        );
    }
}

Match.propTypes = {};

Match.defaultProps = {};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Match);