// @flow
const constants = {
    AUTH: {
        AUTH_URL: '/login'
    },
    APP: {
        BRAND: 'someCompany'
    }
};

export default constants;