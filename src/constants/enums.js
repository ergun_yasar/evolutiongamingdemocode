// @flow
export const GENDER_TYPES = {
    //MALE, FEMALE, OTHER, UNDISCLOSED
    MALE: 'MALE',
    FEMALE: 'FEMALE',
    OTHER: 'OTHER',
    UNDISCLOSED: 'UNDISCLOSED'
};

export const NAVIGATION_ITEM_TYPES = {
    title: 'title',
    image: 'image',
    icon: 'icon'
};
