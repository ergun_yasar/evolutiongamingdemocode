import update from 'immutability-helper';
import reduxConstants from '../constants/reduxConstants';

const initialState = {
    connections: null,
    entity: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case reduxConstants.ACCOUNT.SET_CONNECTIONS: {
            return update(state, {
                connections: { $set: action.payload.connections }
            });
        }
        case reduxConstants.ACCOUNT.SET_ENTITY: {
            return update(state, {
                entity: { $set: action.payload.entity }
            });
        }
        default:
            return state;
    }
}