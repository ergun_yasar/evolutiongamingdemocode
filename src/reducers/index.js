// @flow
import {combineReducers} from 'redux';

import accountReducer from './accountReducer';
import authenticationReducer from './authenticationReducer';

export default combineReducers({
    account: accountReducer,
    authentication: authenticationReducer,
});