import update from 'immutability-helper';
import reduxConstants from '../constants/reduxConstants';

const initialState = {
    entityId: 0,
    sessionId: '',
    ttl: 0,
    isAuthenticated: false,
    isSessionValidityChecked: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case reduxConstants.AUTHENTICATION.SET_IS_AUTHENTICATED: {
            return update(state, {
               isAuthenticated: { $set: action.payload.isAuthenticated }
            });
        }
        case reduxConstants.AUTHENTICATION.SET_IS_SESSION_VALIDITY_CHECKED: {
            return update(state, {
                isSessionValidityChecked: { $set: action.payload.isSessionValidityChecked }
            });
        }
        case reduxConstants.AUTHENTICATION.RESET_SESSION_PROPERTIES: {
            return update(state, {
                entityId: {$set: 0},
                sessionId: {$set: ''},
                ttl: {$set: 0},
            });
        }
        case reduxConstants.AUTHENTICATION.SET_SESSION_PROPERTIES: {
            return update(state, {
                entityId: {$set: action.payload.entityId},
                sessionId: {$set: action.payload.sessionId},
                ttl: {$set: action.payload.ttl},
            });
        }
        default: {
            return state;
        }
    }
}