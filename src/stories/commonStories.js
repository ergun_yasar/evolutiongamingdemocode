// @flow
import React from 'react';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';
import backgrounds from "@storybook/addon-backgrounds";
import centered from '@storybook/addon-centered';

import '../main.scss';
import ContactInformationItem from "../components/common/contactInformationItem";

import SuitcaseIcon from '../assets/icons/suitcase.png';
import BuildingIcon from '../assets/icons/building.png';
import ArrowDown from '../assets/icons/arrow_down_blue.png';
import TrophyIcon from '../assets/icons/trophy.png';
import TrophyWhiteIcon from '../assets/icons/trophy_white.png';
import HelpingHandWhiteIcon from '../assets/icons/helpinghand_white.png';
import UserGropuWhiteIcon from '../assets/icons/usergroup_white.png';

import BorderedButton from "../components/common/borderedButton";

import theme from '../theme/theme';
import Chip from "../components/common/chip";
import HeaderNavigation from "../components/header/headerNavigation";
import HeaderContainer from "../components/header/headerContainer";
import InformationItem from "../components/common/informationItem";
import SmallEventItem from "../components/common/smallEventItem";
import EventDate from "../components/common/eventDate";
import MediumEventItem from "../components/common/mediumEventItem";
import BasicUserInformationItem from "../components/common/basicUserInformationItem";
import HighlightItem from "../components/common/highlightItem";
import ExhibitorItem from "../components/common/exhibitorItem";
import NetworkConnectionItem from "../components/common/networkConnectionItem";
import NotificationItem from "../components/common/notificationItem";
import Select from "../components/common/select/select";
import Alert from "../components/common/alert";
import LargeEventItem from "../components/common/largeEventItem";

const {DUST_GRAY, MIRAGE, DODGER_BLUE} = theme.colors;

storiesOf('Common/Alert', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '968px'}}>
        <Alert
            imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
            label={'accepted your request'}
            strongLabel={'Babak Heydari'}
        />
    </div>
));

storiesOf('Common/Basic Information Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '400px'}}>
        <BasicUserInformationItem
            imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
            name={'Babak Heydari'}
            title={'CEO & Founder'}
            domain={'someCompany'}
        />
    </div>
));

storiesOf('Common/Buttons/Bordered Button', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('Bordered Button with Icon', () => (
    <BorderedButton label={'Update Profile'} height={'42px'} borderRadius={'11px'} iconSource={ArrowDown}/>
))
.add('Bordered Button without icon', () => (
    <BorderedButton label={'Add Interest'} height={'38px'} borderRadius={'19px'}/>
))
.add('Bordered Button with custom color and font size', () => (
    <BorderedButton
        label={'Connect Company'}
        height={'54px'}
        borderRadius={'27px'}
        borderColor={DUST_GRAY}
        labelColor={MIRAGE}
        labelFontSize={'1.25em'}
    />
))
;

storiesOf('Common/Chip', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <Chip label={'Oil & gas'}/>
))
.add('removable', () => (
    <Chip label={'Blockchain'} isRemovable={true}/>
));

storiesOf('Common/Contact Information  Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <ContactInformationItem iconSource={SuitcaseIcon} label={'CEO & Founder'}/>
));

storiesOf('Common/Events/Event Date', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <EventDate day={'21'} month={'SEP'}/>
));


storiesOf('Common/Events/Large Event Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '800px'}}>
        <LargeEventItem
            date={'NOVEMBER 15, 2017 - 8:00 AM'}
            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
            name={'Summer Series/Bret 4 Aug Amsterdam'}
            detail={'Free order #695935231 on November 9, 2017'}
        />
    </div>
));

storiesOf('Common/Events/Medium Event Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <MediumEventItem
        date={'Thu, Oct 11, 8:30am'}
        imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
        location={'KWORKS Entrepreneurship Research Center, Şişli, İstanbul'}
        name={'Summer Series/Bret 4 Aug Amsterdam'}
    />
));

storiesOf('Common/Events/Small Event Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '800px'}}>
        <SmallEventItem
            imageSource={'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F50043117%2F164677687699%2F1%2Foriginal.jpg?h=512&w=512&auto=compress&rect=0%2C93%2C8000%2C4000&s=2b63e2df823f70fbcaa8e3a2a1a062f8'}
            name={'Summer Series/Bret 4 Aug Amsterdam'}
        />
    </div>
));

storiesOf('Common/Exhibitor Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{maxWidth: '800px'}}>
        <ExhibitorItem
            imageSource={require('../assets/images/exhibitors/ing.png')}
            subTitle={'Suistanable Economy'}
            title={'ING Week van de Ondernemers'}
        />
    </div>
));

storiesOf('Common/Highlight Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '800px'}}>
        <HighlightItem
            backgroundColor={DODGER_BLUE}
            iconSource={HelpingHandWhiteIcon}
            label={'You have someCompanyn hands with '}
            strongLabel={'27 People'}
        />
    </div>
));

storiesOf('Common/Information Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{maxWidth: '400px'}}>
        <InformationItem iconSource={TrophyIcon} label={'You have 8 trophies.'}/>
    </div>
));


storiesOf('Common/Network Connection Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{maxWidth: '400px'}}>
        <NetworkConnectionItem
            imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
            name={'Babak Heydari'}
            title={'CEO & Founder'}
            domain={'someCompany'}
        />
    </div>
));

storiesOf('Common/Notification Item', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{maxWidth: '400px', backgroundColor: '#FFF'}}>
        <NotificationItem
            imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
            title={'Babak Heydari'}
            text={'requested to see your contact details'}
        />
    </div>
));

storiesOf('Common/Select', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <div style={{width: '400px', backgroundColor: '#FFF'}}>
        <Select
            options={[{key: '0', label: '0'}, {key: '1', label: '1'}, {key: '2', label: '2'}, {
                key: '3',
                label: '3'
            }, {key: '4', label: '4'}, {key: '5', label: '5'}, {key: '6', label: '6'},]}
        />
    </div>
))
.add('with placeholder option', () => (
    <div style={{width: '400px', backgroundColor: '#FFF'}}>
        <Select
            options={[{key: '0', label: '0'}, {key: '1', label: '1'}, {key: '2', label: '2'}, {
                key: '3',
                label: '3'
            }, {key: '4', label: '4'}, {key: '5', label: '5'}, {key: '6', label: '6'},]}
            showPlaceholderOption={true}
            placeholder={'Please select...'}
        />
    </div>
));

storiesOf('Header/Default', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    {name: "twitter", value: "#00aced"},
    {name: "facebook", value: "#3b5998"},
    {name: "someCompany", value: "#F5F7FA"}
]))
.add('default', () => (
    <HeaderContainer/>
));

