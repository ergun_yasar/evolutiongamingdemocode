// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import backgrounds from "@storybook/addon-backgrounds";
import centered from '@storybook/addon-centered';

import '../main.scss';

import theme from '../theme/theme';
import AcceptAndDeclineButtons from "../components/buttons/acceptAndDeclineButtons";
import Card from "../components/common/card";
import CardDivider from "../components/common/cardDivider";
import ProfileInformationCard from "../components/cards/profileInformationCard/profileInformationCard";
import ProfileStrengthCard from "../components/cards/profileStrengthCard/profileStrengthCard";
import InterestsCard from "../components/cards/interestsCard/interestsCard";
import SocialConnectCard from "../components/cards/socialConnectCard/socialConnectCard";
import InterestingEventsCard from "../components/cards/interestingEventsCard/interestingEventsCard";
import RecentEventsCard from "../components/cards/recentEventsCard/recentEventsCard";
import UserInformationCard from "../components/cards/userInformationCard/userInformationCard";
import LatestActivitiesCard from "../components/cards/latestActivitiesCard/latestActivitiesCard";
import NotificationsCard from "../components/cards/notificationsCard/notificationsCard";
import MatchCard from "../components/cards/matchCard/matchCard";

const { DUST_GRAY, MIRAGE } = theme.colors;

storiesOf('Cards/Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add("Default", () => (<div style={{ height: '500px', width: '500px' }}>
        <Card>
            <CardDivider />
        </Card>
    </div>));

storiesOf('Cards/Interests Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <div style={{ width: '800px' }}>
            <InterestsCard />
        </div>
    ));

storiesOf('Cards/Interesting Events Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <div style={{ width: '800px' }}>
            <InterestingEventsCard />
        </div>
    ));

storiesOf('Cards/Latest Activities Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <div style={{ width: '800px' }}>
            <LatestActivitiesCard />
        </div>
    ));

storiesOf('Cards/Match Card', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    { name: "twitter", value: "#00aced" },
    { name: "facebook", value: "#3b5998" },
    { name: "someCompany", value: "#F5F7FA"}
]))
.add('Default', () => (
    <div style={{ width: '800px' }}>
        <MatchCard
            imageSource={'https://randomuser.me/api/portraits/men/4.jpg'}
            name={'Menno van der Reek'}
            title={'CEO & Founder'}
            domain={'someCompany'}
            slogan={'Looking for interesting partners in the event industry'}
        />
    </div>
));

storiesOf('Cards/Notifications Card', module)
.addDecorator(centered)
.addDecorator(backgrounds([
    { name: "twitter", value: "#00aced" },
    { name: "facebook", value: "#3b5998" },
    { name: "someCompany", value: "#F5F7FA"}
]))
.add('Default', () => (
    <NotificationsCard/>
));


storiesOf('Cards/Profile Information Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add("Default", () => (<div style={{ width: '800px' }}>
        <ProfileInformationCard
            profileImage={'https://randomuser.me/api/portraits/men/9.jpg'}
            name={'Babak Heydari,'}
            title={'CEO & Founder at'}
            domain={'someCompany'}
            slogan={'“Looking for interesting partners in the event industry”'}/>
    </div>));

storiesOf('Cards/Profile Strength Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add("Default", () => (<div style={{ width: '800px' }}>
        <ProfileStrengthCard
            strengthLevel={'All-star profile'}
            currentStep={3}
            totalSteps={5}
            title={'CEO  & Founder'}
            domain={'someCompany'}
            phoneNumber={'+31635467290'}
            emailAddress={'babak@someCompany.com'}
        />
    </div>));

storiesOf('Cards/Recent Events Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <div style={{ width: '800px' }}>
            <RecentEventsCard />
        </div>
    ));

storiesOf('Cards/Social Connect Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <div style={{ width: '800px' }}>
            <SocialConnectCard />
        </div>
    ));

storiesOf('Cards/User Information Card', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Default', () => (
        <UserInformationCard
            domain={'someCompany'}
            imageSource={'https://randomuser.me/api/portraits/men/9.jpg'}
            name={'Babak Heydari'}
            title={'CEO & Founder'}
        />
    ));


