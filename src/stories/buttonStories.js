// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import backgrounds from "@storybook/addon-backgrounds";
import centered from '@storybook/addon-centered';

import '../main.scss';

import theme from '../theme/theme';
import AcceptAndDeclineButtons from "../components/buttons/acceptAndDeclineButtons";
import SocialMediaButton from "../components/common/socialMediaButton";
import { faLinkedinIn, faFacebookF } from '@fortawesome/free-brands-svg-icons';


import FacebookIcon from '../assets/icons/facebook.png';

const { DUST_GRAY, MIRAGE, TORY_BLUE, DENIM, SAN_MARINO, ROYAL_BLUE } = theme.colors;


storiesOf('Buttons/Accept And Decline Buttons', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('default', () => (
        <AcceptAndDeclineButtons />
    ));

storiesOf('Buttons/Social Media Buttons', module)
    .addDecorator(centered)
    .addDecorator(backgrounds([
        { name: "twitter", value: "#00aced" },
        { name: "facebook", value: "#3b5998" },
        { name: "someCompany", value: "#F5F7FA"}
    ]))
    .add('Linked In', () => (
        <SocialMediaButton iconSource={faLinkedinIn} iconBackgroundColor={TORY_BLUE} label={'Connect'} labelBackgroundColor={DENIM}/>
    ))
    .add('Facebook', () => (
        <SocialMediaButton iconSource={faFacebookF} iconBackgroundColor={SAN_MARINO} label={'Connect'} labelBackgroundColor={ROYAL_BLUE}/>
    ));
