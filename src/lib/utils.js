export const isLocalStorageAvailable = () => {
    //Modernizr approach
    const test = 'test';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch (e) {
        return false;
    }
};

export const replaceVariableInUrlPath = (url, replaceValue, variable = '{id}') => {
    return url.replace(variable, replaceValue);
};