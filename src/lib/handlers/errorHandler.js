// @flow
import {ErrorModel} from '../../models/sharedModels';
import store from '../../store/someCompanyOnStore';
import authenticationActionCreator from '../../actionCreators/authenticationActionCreator';
import errorActionCreator from '../../actionCreators/errorActionCreator';

import {HTTP_STATUS_CODES} from '../../constants/httpStatusCodes';

class ErrorHandler {
    error: ErrorModel;

    constructor(error: ErrorModel) {
        this.error = Object.assign({}, error);
    }

    logError = () => {

    };

    manageError = () => {
        this.logError();
        store.dispatch(errorActionCreator.manageError(this.error));
        if (this.error.status === HTTP_STATUS_CODES.FORBIDDEN) {
            store.dispatch(authenticationActionCreator.logout());
        }
    }
}

export default ErrorHandler;