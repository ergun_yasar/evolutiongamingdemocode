// @flow
import type {LoginModel, LoginResponseModel} from '../models/authenticationModels';
import type {Dispatch, ActionCreator} from 'redux';

import PersonApi from '../services/api/personApi';
import reduxConstants from '../constants/reduxConstants';
import {isLocalStorageAvailable} from '../lib/utils';
import {AssumeModel, AssumeResponseModel} from '../models/authenticationModels';
import {AuthenticatedRequestModel} from '../models/sharedModels';

type SessionInformation = {
    entityId: number,
    sessionId: string,
    ttl: number
}

const authenticateSession = (dispatch, responseModel: AssumeResponseModel | LoginResponseModel): void => {
    const {entity_id, session_id, ttl} = responseModel;
    const data: any = responseModel.data;
    dispatch({
        type: reduxConstants.ACCOUNT.SET_CONNECTIONS,
        payload: {
            connections: data.connections
        }
    });
    dispatch({
        type: reduxConstants.ACCOUNT.SET_ENTITY,
        payload: {
            entity: data.entity
        }
    });
    dispatch({
        type: reduxConstants.AUTHENTICATION.SET_IS_AUTHENTICATED,
        payload: {
            isAuthenticated: true
        }
    });
    dispatch({
        type: reduxConstants.AUTHENTICATION.SET_SESSION_PROPERTIES,
        payload: {
            entityId: entity_id,
            sessionId: session_id,
            ttl: ttl,
        }
    });
    dispatch({
        type: reduxConstants.AUTHENTICATION.SET_IS_SESSION_VALIDITY_CHECKED,
        payload: {isSessionValidityChecked: true}
    });
    storeSessionInformation(entity_id, session_id, ttl);
};

const removeSessionInformation = (): void => {
    localStorage.removeItem('entity_id');
    localStorage.removeItem('session_id');
    localStorage.removeItem('ttl');
};

const retrieveSessionInformation = (): ?SessionInformation => {
    if (isLocalStorageAvailable()) {
        const entityId = localStorage.getItem('entity_id');
        const sessionId = localStorage.getItem('session_id');
        const ttl = localStorage.getItem('ttl');
        try {
            return entityId && sessionId && ttl ? {
                entityId: parseInt(entityId, 10),
                sessionId,
                ttl: parseInt(ttl, 10)
            } : null;
        } catch (e) {
            return null;
        }
    }
    return null;
};

const storeSessionInformation = (entityId: number, sessionId: string, ttl: number): void => {
    if (isLocalStorageAvailable()) {
        localStorage.setItem('entity_id', entityId.toString());
        localStorage.setItem('session_id', sessionId);
        localStorage.setItem('ttl', ttl.toString());
    }
};

const unAuthenticateSession = (dispatch): void => {
    dispatch({
        type: reduxConstants.AUTHENTICATION.SET_IS_AUTHENTICATED,
        payload: {
            isAuthenticated: false
        }
    });
    dispatch({
        type: reduxConstants.AUTHENTICATION.RESET_SESSION_PROPERTIES
    });
    dispatch({
        type: reduxConstants.AUTHENTICATION.SET_IS_SESSION_VALIDITY_CHECKED,
        payload: {isSessionValidityChecked: true}
    });
    removeSessionInformation();
};

const authenticationActionCreator = {
    checkSessionValidity: (): any => (dispatch: Dispatch<any>) => {
        const sessionInformation: ?SessionInformation = retrieveSessionInformation();

        if (sessionInformation) {
            const {entityId, sessionId, ttl} = sessionInformation;
            const now = new Date().valueOf() / 1000;
            if (now <= ttl) {
                const personApi = new PersonApi();
                const assumeModel = new AssumeModel();
                assumeModel.sessionId = sessionId;
                assumeModel.entityId = entityId;
                personApi.assume(assumeModel).then((response: AssumeResponseModel) => {
                    authenticateSession(dispatch, response);
                }).catch((error) => {
                    unAuthenticateSession(dispatch);
                });
            } else {
                unAuthenticateSession(dispatch);
            }
        } else {
            unAuthenticateSession(dispatch);
        }
    },
    login: (email: string, password: string): any => (dispatch: Dispatch<*>) => {
        const personApi = new PersonApi();
        const loginModel: LoginModel = {
            email,
            password,
        };
        personApi.login(loginModel).then((response: LoginResponseModel) => {
            console.log(response);
            authenticateSession(dispatch, response);
        }).catch((error) => {
            console.log(error);
            dispatch({
                type: reduxConstants.AUTHENTICATION.SET_IS_AUTHENTICATED,
                payload: {
                    isAuthenticated: false
                }
            });
        });
    },
    logout: (): any => (dispatch: Dispatch<*>) => {
        unAuthenticateSession(dispatch);
    }
};

export default authenticationActionCreator;