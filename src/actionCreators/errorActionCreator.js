// @flow
import {ErrorModel} from '../models/sharedModels';
import type {Dispatch} from 'redux';

const errorActionCreator = {
    manageError: (error: ErrorModel) => (dispatch: Dispatch<*>) => {

    }
};

export default errorActionCreator;