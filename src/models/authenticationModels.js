// @flow
import {AuthenticatedRequestModel} from './sharedModels';

export type LoginModel = {
    device?: string;
    email?: string;
    password?: string;
    token?: string;
}

export class LoginResponseModel {
    data: ?Object;
    entity_id: number;
    session_id: string;
    ttl: number;
}

export class AssumeModel extends AuthenticatedRequestModel {}

export class AssumeResponseModel extends LoginResponseModel {}

export class WhoamiModel extends AuthenticatedRequestModel {
    entityId: ?number;
}