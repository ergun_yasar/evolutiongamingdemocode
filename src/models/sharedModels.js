export class AuthenticatedRequestModel {
    sessionId: string;
    entityId: number;
}

export class EntityModel {
    
}

export class ErrorModel {
    baseURL: string;
    url: string;
    status: number;
    message: ?string;
    data: ?Object;
}