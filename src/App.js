// @flow
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Link, Switch, withRouter} from 'react-router-dom';
import update from 'immutability-helper';

import store from './store/someCompanyOnStore';

import './main.scss';

import HeaderContainer from './components/header/headerContainer';
import Loading from './components/common/loading/loading';
import PrivateRoute from './components/privateRoute';

import Home from './screens/home';
import Login from './screens/login';
import Network from './screens/network';
import Profile from './screens/profile';
import Requests from './screens/requests';
import Tickets from './screens/tickets';
import Match from './screens/match';

import authenticationActionCreator from './actionCreators/authenticationActionCreator';

const Header = withRouter(props => <HeaderContainer {...props}/>);

type Props = {};

type State = {
    isAuthenticated: boolean;
    isSessionValidityChecked: boolean;
}

//TODO: Remove inline style or move them to sass
class App extends Component<Props, State> {
    constructor() {
        super();
        this.state = {
            isAuthenticated: false,
            isSessionValidityChecked: false,
        };
        store.subscribe(this.handleStoreChange);
    }

    componentDidMount() {
        const storeState: any = store.getState();
        this.setState(previousState => update(previousState, {isSessionValidityChecked: {$set: storeState.authentication.isSessionValidityChecked}}));
        store.dispatch(authenticationActionCreator.checkSessionValidity());
    }

    handleStoreChange = () => {
        const storeState: any = store.getState();
        this.setState(previousState => update(previousState, {
            isSessionValidityChecked: {$set: storeState.authentication.isSessionValidityChecked},
            isAuthenticated: {$set: storeState.authentication.isAuthenticated},
        }));
    };

    renderHeader = () => {
        return (
            <nav className={'fixed-header'}>
                <Header/>
            </nav>
        );
    };

    render() {
        const {isAuthenticated, isSessionValidityChecked} = this.state;
        return (
            <Provider store={store}>
                <Router>
                    <div
                        style={{
                            width: '100%',
                            height: isSessionValidityChecked ? 'calc(100% - 100px)' : '100%',
                            backgroundColor: '#F5F7FA',
                            paddingTop: isSessionValidityChecked ? '100px' : '0'
                        }}
                    >
                        {
                            isAuthenticated && this.renderHeader()
                        }
                        {
                            isSessionValidityChecked ?
                                <div style={{display: 'flex', flex: 1, justifyContent: 'center'}}>
                                    <Switch>
                                        <PrivateRoute exact path={'/'} component={Home} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute exact path={'/someCompany'} component={Home} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute exact path={'/someCompany/'} component={Home} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute path={'/home'} component={Home} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute path={'/profile'} component={Profile} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute path={'/tickets'} component={Tickets} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute path={'/network'} component={Network} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute path={'/match'} component={Match} isAuthenticated={isAuthenticated}/>
                                        <PrivateRoute
                                            path={'/requests'} component={Requests} isAuthenticated={isAuthenticated}
                                        />
                                        <Route path={'/login'} component={Login}/>
                                    </Switch>
                                </div>
                                :
                                <div style={{display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center', height: '100%'}}>
                                    <Loading />
                                </div>
                        }

                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
