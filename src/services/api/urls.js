export default {
    person: {
        delete: {
            _: '/person/{id}'
        },
        get: {
            self: '/person',
            _: '/person/{id}',
            devices: '/person/{id}/devices',
            entity: '/entity/{id}',
            event_attendeeProperty: '/event/{id}/attendeeProperty',
            event_attendees: '/event/{id}/attendees',
            event_match_and_meet: '/event/{id}/match_and_meet',
            events: '/person/{id}/events',
            link_to_linked_in: '/person/link_to_linked_in',
            unsubscribe: '/unsubscribe',
            whoami: '/whoami'
        },
        patch: {
            _: '/person/{id}',
            device_checkin: '/device/{id}/checkin',
            device_checkout: '/device/{id}/checkout',
            password_reset: '/person/password_reset',
            profile_picture: '/person/{id}/profile_picture',
        },
        post: {
            self: '/person',
            assume: '/person/{id}/assume',
            event_attendees: '/event/{id}/attendees',
            login: '/person/login',
            password_reset: '/person/password_reset',
            register: '/person/register'
        }
    }
};