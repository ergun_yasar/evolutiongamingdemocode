// @flow
import type {LoginModel} from '../../models/authenticationModels';
import ApiCall from '../apiCall';
import urls from './urls';
import type {ApiConfig} from '../apiCall.type';
import {AssumeModel, WhoamiModel} from '../../models/authenticationModels';
import {replaceVariableInUrlPath} from '../../lib/utils';
import {AuthenticatedRequestModel} from '../../models/sharedModels';

class PersonApi {
    get = (authenticatedRequestModel: AuthenticatedRequestModel) => {
        console.log(authenticatedRequestModel);
        const apiConfig: ApiConfig = {
            url: replaceVariableInUrlPath(urls.person.get._, authenticatedRequestModel.entityId),
            method: 'GET',
            params: {
                page_size: 20,
                page: 0
            },
            headers: {
                'Authorization': authenticatedRequestModel.sessionId
            }
        };
        return new ApiCall(apiConfig).callApi();
    };

    assume = (assumeModel: AssumeModel) => {
        const apiConfig: ApiConfig = {
            url: replaceVariableInUrlPath(urls.person.post.assume, assumeModel.entityId),
            method: 'POST',
            headers: {
                'Authorization': assumeModel.sessionId
            }
        };
        return new ApiCall(apiConfig).callApi();
    };

    login = (loginModel: LoginModel) => {
        const apiConfig: ApiConfig = {
            url: urls.person.post.login,
            method: 'POST',
            data: loginModel,
        };
        return new ApiCall(apiConfig).callApi();
    };

    whoami = (whoamiModel: WhoamiModel) => {
        const apiConfig: ApiConfig = {
            url: urls.person.get.whoami,
            method: 'GET',
            headers: {
                'Authorization': whoamiModel.sessionId
            }
        };
        return new ApiCall(apiConfig).callApi();
    };
}

export default PersonApi;