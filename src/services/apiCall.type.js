// @flow
export type ApiConfig = {
    baseURL?: string,
    url: string,
    method: string,
    headers?: Object,
    params?: Object,
    data?: Object,
    timeout?: number,
    responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream',
}

export class ApiResponse {

}

export class ApiError {

}