// @flow
import axios from 'axios';
import type {ApiConfig, ApiResponse, ApiError} from './apiCall.type';
import {ErrorModel} from '../models/sharedModels';
import ErrorHandler from '../lib/handlers/errorHandler';
import {HTTP_STATUS_CODES} from '../constants/httpStatusCodes';

const TIMEOUT = 1000;

const BASE_URL = '';
const AuthCheckURL = '/whoami';

class ApiCall {
    config: ApiConfig;

    constructor(config: ApiConfig) {
        this.config = Object.assign({}, config, {baseURL: BASE_URL});
    }

    callApi = (): Promise<any> => {
        // TODO: Make a call before callApi to check authentication
        return new Promise((resolve, reject) => {
            axios(Object.assign({}, this.config)).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                const errorModel = new ErrorModel();
                errorModel.baseURL = error.config.baseURL;
                errorModel.url = error.config.url;

                if (error.response) {
                    errorModel.data = error.response.data;
                    errorModel.status = error.response.status;
                } else if (error.request) {
                    errorModel.status = HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR;
                } else {
                    errorModel.message = error.message;
                    errorModel.status = HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR;
                }

                const errorHandler = new ErrorHandler(errorModel);
                errorHandler.manageError();
                reject(errorModel);
            });
        });
    }
}

export default ApiCall;