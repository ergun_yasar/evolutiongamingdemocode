const path = require("path");

var sass = require("node-sass");
var sassUtils = require("node-sass-utils")(sass);

const sassVars = require(__dirname + "/../src/theme/theme.js")

module.exports = {
    module: {
        rules: [
            {
                test: /\.scss$/,
                loaders: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader",
                    options: {
                        functions: {
                            "get($keys)": function(keys) {
                                keys = keys.getValue().split(".");
                                let result = sassVars;
                                let i;
                                for (i = 0; i < keys.length; i++) {
                                    result = result[keys[i]];
                                }
                                result = sassUtils.castToSass(result);
                                return result;
                            }
                        }
                    }
                }],
                include: path.resolve(__dirname, "../"),
            },
            {
                test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: require.resolve('url-loader'),
                options: {
                    limit: 10000,
                    name: 'static/media/[name].[hash:8].[ext]',
                },
            },
            {
                test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: require.resolve('file-loader'),
                    options: {
                        name: 'fonts/[name].[ext]',
                    }
                }],
                include: path.resolve(__dirname, "../"),
            },
        ]
    }
};